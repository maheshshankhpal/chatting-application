/*
Name    :-  chitchatserver.java
Authors :-  Prerana Patil & Swati Patil

Classes Included :-
	# Class chitchattoclients
	# Class chitchatvulture
	# Class chirchatserver
*/


import java.net.*;
import java.util.*;
import java.io.*;


class chitchattoclients
{
    protected chitchatserver myparent;


    public chitchattoclients(chitchatserver parent)
    {
	myparent = parent;
    }

    public void clientoutput(String outputstring)
    {
	for (int count = 0; count < myparent.connections.size(); count ++)
	    {
                chitchatclientsocket c = 
                    ((chitchatclientsocket) 
		     myparent.connections.elementAt(count));
        
		if (c == null)
		    {   
			myparent.Serveroutput("Error managing clients!\n");
			System.exit(1);
		    }
                
		c.myoutput(outputstring);
	    }
    }
}


class chitchatvulture
    extends Thread
{
    protected chitchatserver myparent;
    protected boolean stop = false;


    public chitchatvulture(chitchatserver parent)
    {
        super("Chit-Chat thread vulture");
	myparent = parent;
	start();
    }

    public void run()
    {
	int count;
        chitchatclientsocket temp;

	while(!stop)
	    {
		for (count = 0; count < myparent.connections.size();
		     count ++)
		    {
                        temp = (chitchatclientsocket) 
			    myparent.connections.elementAt(count);

			if (temp.isAlive() == false)
			    {
				myparent.disconnect(temp, false);
			    }
		    }

		yield();
		try {
		    sleep(1000);
		}
		catch(InterruptedException E) 
		    {
			// The server should shut down now.
			myparent.Shutdown();
			return;
		    }
	    }
    }
}


public class chitchatserver
    extends Thread
{
    protected int port;
    protected chitchatserverwindow mywindow;
    protected boolean graphics;
    protected static int DEFAULTPORT = 12468;

    protected ServerSocket serversocket;
    protected Socket socket;
    protected ThreadGroup threadgroup;
    protected chitchatvulture thevulture;

    public Vector connections;
    public Vector messages;
    public chitchattoclients thewriter;

    protected File Logfile;
    protected long LogfileSize = 0;
    private FileOutputStream Log;
    
    protected int currentconnections = 0;
    protected int peakconnections = 0;
    protected int totalconnections = 0;
    
    protected boolean stop = false;


    public chitchatserver(int askport, boolean isgraphics)
    {
        super("Chit-Chat server");

	port = askport;
	graphics = isgraphics;

	// if user wants graphics, set up simple window

	if (graphics)
	    {
		mywindow =
                    new chitchatserverwindow(this, "Chit-Chat v"
                                            + chitchat.VERSION + " Server");
		mywindow.setSize(400, 400);
		mywindow.setVisible(true);
	    }

	else
	    {
                System.out.println("\nChit-Chat server status");
		System.out.println("Listening on port " + port);
		System.out.println("Connections:");
	    }

	// open up the log file
	try {
	    Logfile = new File("Server.log");
	    LogfileSize = Logfile.length();        
	    Log = new FileOutputStream(Logfile);

	} 
	catch (IOException F) { 
	    Serveroutput("Unable to open log file\n"); 
	}

	try {
	    serversocket = new ServerSocket(port);
	} 
	catch (IOException e) {
	    Serveroutput("Couldn't create server socket\n");
	    System.exit(1);
	}

	connections = new Vector();
	messages = new Vector();
        thewriter = new chitchattoclients(this);
	threadgroup = new ThreadGroup("Clients");
        thevulture = new chitchatvulture(this);

	Serveroutput("Reading message file\n");
	ReadMessages();

	Serveroutput("Waiting for connections\n");
	start();
    }


    public synchronized void disconnect(chitchatclientsocket who,
					boolean notify)
    {
	int count;

	for (count = 0; count < connections.size(); count ++)
	    {
                chitchatclientsocket temp =
                    ((chitchatclientsocket) connections.elementAt(count));

		if (temp == who)
		    {
			if (notify)
			    {
				// Try to let the user know they're being
				// disconnected 
				temp.myoutput("/SERVERMESSAGE " + "You are "
				      + "being disconnected.  Goodbye.\n");
			    }
			
			// Stop the thread for this client
			temp.stop = true;

			try {
			    temp.mysocket.close();
			} 
			catch (IOException F) {
			    Serveroutput("Couldn't close socket\n");
			}

			connections.removeElement(temp);
			connections.trimToSize();

			Serveroutput("User " + temp.username 
				     + " disconnected\n"); 

			currentconnections = connections.size();
			Serveroutput("There are " +
				     currentconnections +
				     " users connected\n");

			// Tell all the other clients to remove this user
			thewriter.clientoutput("/REMOVEUSER " + who.username
					       + "\n");

			// Remove the client reader
			temp = null;

			try { 
			    sleep(250); 
			} 
			catch (InterruptedException I)
			    {
				// The server should shut down now.
				Shutdown();
				return;
			    }

			break;
		    }
	    }

	if (graphics)
	    {
		synchronized (mywindow.userlist) {
		    // Remove the user name from the list .
		    for (count = 0;
			 count < mywindow.userlist.getItemCount();
			 count ++)
			{
			    if (mywindow.userlist.getItem(count).equals(who.username))
				{
				    mywindow.userlist.remove(who.username);
				    break;
				}
			}
		}
		
		mywindow.updatestats();

		if (connections.size() <= 1)
		    mywindow.disconnectall.setEnabled(false);
		if (connections.size() <= 0)
		    mywindow.disconnect.setEnabled(false);
	    }

	return;
    }


    public synchronized void disconnectall(boolean notify)
    {
	int count;

	// Loop backwards through all of the current connections
	for(count = (connections.size() - 1); count >= 0; count --)
	    {
                chitchatclientsocket temp =
                    (chitchatclientsocket) connections.elementAt(count);

		if (temp == null)
		    continue;

		disconnect(temp, notify);
	    }
	return;
    }


    public void run()
    {
	while (!stop) 
	    {
		try {
		    socket = serversocket.accept();
		} 
		catch (IOException e) { 
		    Serveroutput("Connection error\n");
		    try {
			serversocket.close();
		    } 
		    catch (IOException f) {
			Serveroutput("Couldn't close socket\n");
		    }
		    System.exit(1);
		}

		if (socket == null)
		    {
			Serveroutput("Server tried to start up NULL "
				     + "socket\n");
			try {
			    serversocket.close();
			} 
			catch (IOException g) {
			    Serveroutput("Couldn't close socket\n");
			}
			System.exit(1);
		    }

                chitchatclientsocket cs = 
                    new chitchatclientsocket(this, socket, threadgroup);
		cs.start();

		synchronized (connections) 
		    {
			connections.addElement(cs);
		    }

		if (connections.size() == 0)
		    {
			Serveroutput("Failed to add new client to list\n");
			try {
			    serversocket.close();
			} 
			catch (IOException h) {
			    Serveroutput("Couldn't close socket\n");
			}
			System.exit(1);
		    }

		// Update the statistics
		currentconnections = connections.size();
		totalconnections++;
		if (currentconnections > peakconnections)
		    peakconnections = currentconnections;

		if (graphics)
		    mywindow.updatestats();
	    }
    }

    protected void Serveroutput(String message) 
    {
	if (graphics)
	    mywindow.logwindow.append(message);
	else
	    System.out.print(message);

	// now write it to the log

	try {
	    byte[] messagebytes = message.getBytes();
	    Log.write(messagebytes);
	    LogfileSize += message.length();
	} 
	catch (IOException F) {
	    if (graphics)
		mywindow.logwindow.append("Unable to write to log file\n");
	    else
		System.out.print("Unable to write to log file\n");
	}

	return;
    }

    protected void ReadMessages()
    {
	String amessage;
	int count;
	File messagefile;
	long FileSize;
	byte[] buffer;
	String BufferString;
	FileInputStream themessages;
	String TempFor;
	String TempFrom;
	String TempMessage;

	try {
	    messagefile = new File("Messages.saved");
	    FileSize = messagefile.length();
	    themessages = new FileInputStream(messagefile);
	    
	    // allocate a byte array for the file
	    buffer = new byte[(int) FileSize];

	    // read in the file
	    themessages.read(buffer);

	    // convert to a string
	    BufferString = new String(buffer);
	    
	    // now go through and get the messages
	    for (count = 0; count < BufferString.length(); count ++) 
		{
		    while (BufferString.charAt(count) != '/') 
			count ++;

		    BufferString = BufferString.substring(count);
		    count = 0;
		    if (BufferString.startsWith("/FOR") == false)
			{
			    if (count >= BufferString.length()) continue;
			    Serveroutput("Message file is corrupt\n");
			    return;
			}

		    BufferString = BufferString.substring(5);
		    count = 0;
		    TempFor = new String("");
		    while (BufferString.charAt(count) != '/') 
			{
			    TempFor += BufferString.charAt(count);
			    count ++;
			}

		    BufferString = BufferString.substring(count);
		    count = 0;
		    if (BufferString.startsWith("/FROM") == false)
			{
			    if (count >= BufferString.length()) continue;
			    Serveroutput("Message file is corrupt\n");
			    return;
			}

		    BufferString = BufferString.substring(6);
		    count = 0;
		    TempFrom = new String("");
		    while (BufferString.charAt(count) != '/') 
			{
			    TempFrom += BufferString.charAt(count);
			    count ++;
			}

		    BufferString = BufferString.substring(count);
		    count = 0;
		    if (BufferString.startsWith("/MESSAGE") == false) 
			{
			    if (count >= BufferString.length()) continue;
			    Serveroutput("Message file is corrupt\n");
			    return;
			}
		    BufferString = BufferString.substring(9);
		    count = 0;
		    TempMessage = new String("");
		    while (BufferString.charAt(count) != '/')
			{
			    if (count >= BufferString.length()) continue;
			    TempMessage += BufferString.charAt(count);
			    count ++;
			}
		    BufferString = BufferString.substring(count + 3);
		    count = 0;
		    

                    messages.addElement(new chitchatmessage(TempFor, TempFrom,
							   TempMessage));
		}
	} 
	catch (IOException E) {
	    return;
	}

	return;
    }

    protected void SaveMessages()
    {
	String amessage;
	int count;
	File messagefile;
	FileOutputStream themessages;

	try {
	    messagefile = new File("Messages.saved");
	    themessages = new FileOutputStream(messagefile);

	    for (count = 0; count < messages.size(); count ++)
		{
		    amessage = 
			new String(messages.elementAt(count).toString());
		    byte[] miscbyte = amessage.getBytes();
		    themessages.write(miscbyte);
		}

	    themessages.close();
	} 
	catch (IOException E) {
	    Serveroutput("Unable to create message file\n"); 
	    return;
	}

	return;
    }

    protected void Shutdown()
    {
	Serveroutput("Server shutting down...\n");

	thewriter.clientoutput("/SERVERMESSAGE Server is shutting down.\n");

	if (connections.size() > 0)
	    {
		Serveroutput("Disconnecting users\n");
		disconnectall(true);
	    }
	else
	    Serveroutput("No users connected\n");

	// Shut down the vulture
	thevulture.stop = true;

	// Print some stats
	Serveroutput("Peak connections this session: "
		     + peakconnections + "\n");
	Serveroutput("Total connections this session: "
		     + totalconnections + "\n");

	Serveroutput("Saving user messages\n");
	SaveMessages();

	Serveroutput("Closing log file\n");
	try {
	    Log.close();
	} 
	catch (IOException F) { 
	    Serveroutput("Unable to close server log file\n"); 
	}

	if (graphics)
	    mywindow.dispose();

	stop = true;

	// Feedback if GUI is not used
	if (!graphics)
	    {
		System.out.println("");
                System.out.println("Chit-Chat server shutdown complete");
	    }

	System.exit(0);
    }

    private static void usage()
    {
        System.out.println("\nChit-Chat server usage:");
        System.out.println("java chitchatserver [-portnumber number] "
			   + "[-nographics]");
	return;
    }

    public static void main(String[] args)
    {
	int useport = DEFAULTPORT;
	boolean usegraphics = true;

	// Parse the arguments
	for (int count = 0; count < args.length; count ++)
	    {
		if (args[count].equals("-portnumber"))
		    {
			if (++count < args.length)
			    {
				try {
				    useport = Integer.parseInt(args[count]);
				}
				catch (Exception E) {
                                    System.out.println("\nchitchatserver: "
					       + "illegal port number "
					       + args[count]);
				    System.out.println("Type 'java "
                                               + "chitchatserver -help' "
					       + "for usage information");
				    System.exit(1);
				}			
			    }
		    }

		else if (args[count].equals("-nographics"))
		    usegraphics = false;

		else if (args[count].equals("-help"))
		    {
			usage();
			System.exit(1);
		    }

		else
		    {
                        System.out.println("\nchitchatserver: unknown "
					   + "argument " + args[count]);
                        System.out.println("Type 'java chitchatserver -help' "
					   + "for usage information");
			System.exit(1);
		    }
	    }

	// Start the server
        new chitchatserver(useport, usegraphics);
	return;
    }
}
