/*
Name    :-  chitchatwindow.java


Classes Included :-
	# Class chitchatwindow
*/


import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.net.*;
import java.applet.*;


public class chitchatwindow
    extends Frame
    implements ActionListener, ItemListener, KeyListener, MouseListener, 
	       WindowListener
{
    public String name = new String("");
    public String location = new String("");
    public String host = new String("localhost");
    public String port = new String("12468");
    public String whofor = new String("ALL");
    public int portnumber = 12468;
    public String additional = new String();
    protected boolean locksettings = false;
    public String drawfont;
    public int drawstyle;
    public int drawsize;
    public String drawtext;
    public URL chitchatURL;
    protected String buffer;
    protected int OverallSizex;
    protected int OverallSizey;

    public GridBagLayout mylayout = new GridBagLayout();
    public GridBagConstraints myconstraints = new GridBagConstraints();
    Font smallfont = new Font("Helvetica", Font.PLAIN, 12);
    Font largefont = new Font("Helvetica", Font.PLAIN, 14);

    // socket stuff

    public chitchatclient myclient;
    protected boolean connected;

    // the menu items

    public MenuItem connect;
    public MenuItem disconnect;
    protected MenuItem buggeroff;
    protected Menu connections;
    protected MenuItem menupage;
    protected MenuItem menuleavemess;
    protected MenuItem menuviewmess;
    protected MenuItem menuclear;
    protected Menu actions;
    protected MenuItem copy;
    protected MenuItem paste;
    protected Menu edit; 
    protected MenuItem settings;
    protected MenuItem viewadditional;
    protected CheckboxMenuItem playsound;
    protected CheckboxMenuItem showcanvas;
    protected CheckboxMenuItem showmessages;
    protected Menu view;
    protected MenuItem manual;
    protected MenuItem about;
    protected Menu help;
 
    // panel 1

    protected Label sendline;
    protected Label conference;
    protected TextArea typed;
    protected TextArea messages;

    // panel 2

    protected Label drawcanvas;
    public chitchatcanvas canvas;

    // panel 3

    protected Label drawingcontrols;
    protected Button clearcanvas;
    protected Choice filltype;
    protected Choice thickness;
    protected Choice colorchoice;
    protected Checkbox freehand;
    protected Checkbox line;
    protected Checkbox oval;
    protected Checkbox rectangle;
    protected Checkbox text;
    protected CheckboxGroup drawtype;

    // panel 4

    protected Label namelabel;
    public TextField userid;
    protected Label sendtolabel;
    public java.awt.List sendto;
    protected Label activitylabel;
    protected TextField activity;
    protected Button whosthis;
    protected Button page;
    protected Button message;
    protected Button readmess;

    protected Panel p1;
    protected Panel p2;

    // set up

    public chitchatwindow(String userName, String hostName, String portName,
			 boolean showMessages, boolean showCanvas, URL myURL)
    {
	super();

	// Set the username, host, and port values if they've been specified
	if ((userName != null) && (!userName.equals("")))
	    name = userName;
	if ((hostName != null) && (!hostName.equals("")))
	    host = hostName;
	if ((portName != null) && (!portName.equals("")))
	    port = portName;

	// Get the URL of the current directory, so that we can find the
	// rest of our files.
        chitchatURL = myURL;

	// set background color

	Color mycolor = Color.lightGray;
	setBackground(mycolor);
	connected = false;

	// set default font

	drawfont = "Helvetica";
	drawstyle = Font.PLAIN;
	drawsize = 10;

	OverallSizex = 600;
	OverallSizey = 500;
	setSize(OverallSizex, OverallSizey);
	myconstraints.fill = myconstraints.BOTH;

	// set up all of the window crap

	setLayout(mylayout);
	myconstraints.insets = new Insets(0,5,0,5);

	// the menu bar

	connect = new MenuItem("Connect");
	connect.addActionListener(this);
	connect.setEnabled(true);

	disconnect = new MenuItem("Disconnect");
	disconnect.addActionListener(this);
	disconnect.setEnabled(false);

        buggeroff = new MenuItem("Exit");
	buggeroff.addActionListener(this);
	buggeroff.setEnabled(true);

	connections = new Menu("Connections");
	connections.add(connect);
	connections.add(disconnect);
	connections.add(buggeroff);

	menupage = new MenuItem("Page user(s)");
	menupage.addActionListener(this);
	menupage.setEnabled(false);

	menuleavemess = new MenuItem("Leave message");
	menuleavemess.addActionListener(this);
	menuleavemess.setEnabled(false);

	menuviewmess = new MenuItem("Read message(s)");
	menuviewmess.addActionListener(this);
	menuviewmess.setEnabled(false);

	menuclear = new MenuItem("Clear canvas");
	menuclear.addActionListener(this);
	menuclear.setEnabled(true);

	actions = new Menu("Actions");
	actions.add(menupage);
	actions.add(menuleavemess);
	actions.add(menuviewmess);
	actions.add(menuclear);

	copy = new MenuItem("Copy");
	copy.addActionListener(this);
	copy.setEnabled(true);

	paste = new MenuItem("Paste");
	paste.addActionListener(this);
	paste.setEnabled(false);

	edit = new Menu("Edit");
	edit.add(copy);
	edit.add(paste);

	settings = new MenuItem("Connection settings");
	settings.addActionListener(this);
	settings.setEnabled(true);

	viewadditional = new MenuItem("Who's this?");
	viewadditional.addActionListener(this);
	viewadditional.setEnabled(false);

	playsound = new CheckboxMenuItem("Play sound when paged");
	playsound.addItemListener(this);
	playsound.setEnabled(true);
	playsound.setState(true);

	showmessages = new CheckboxMenuItem("Show conference (chat) text");
	showmessages.addItemListener(this);
	showmessages.setEnabled(true);
	showmessages.setState(showMessages);

	showcanvas = new CheckboxMenuItem("Show drawing canvas");
	showcanvas.addItemListener(this);
	showcanvas.setEnabled(true);
	showcanvas.setState(showCanvas);

	view = new Menu("View");
	view.add(settings);
	view.add(viewadditional);
	view.add(playsound);
	view.add(showmessages);
	view.add(showcanvas);

	manual = new MenuItem("Manual");
	manual.addActionListener(this);
	manual.setEnabled(true);

        about = new MenuItem("About Chit-Chat");
	about.addActionListener(this);
	about.setEnabled(true);

	help = new Menu("Help");
	help.add(manual);
	help.add(about);

	MenuBar menubar = new MenuBar();
	menubar.add(connections);
	menubar.add(actions);
	menubar.add(edit);
	menubar.add(view);
	menubar.add(help);
	menubar.setHelpMenu(help);
	setMenuBar(menubar);

        // the first panel

	p1 = new Panel();
	p1.setLayout(mylayout);

	sendline = new Label("Text lines to send:");
	sendline.setFont(smallfont);
	myconstraints.gridx = 1; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(sendline, myconstraints);
	p1.add(sendline);

	typed = new TextArea("", 2, 50, TextArea.SCROLLBARS_VERTICAL_ONLY);
	typed.setEditable(true);
	typed.setFont(largefont);
	typed.addKeyListener(this);
	myconstraints.gridx = 1; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1; myconstraints.weighty = 0;
	mylayout.setConstraints(typed, myconstraints);
	p1.add(typed);

	conference = new Label("Conference text:");
	conference.setFont(smallfont);
	myconstraints.gridx = 1; myconstraints.gridy = 3;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(conference, myconstraints);
	p1.add(conference);

	messages = new TextArea("", 10, 50,
				TextArea.SCROLLBARS_VERTICAL_ONLY);
	messages.setEditable(false);
	messages.setFont(smallfont);
	myconstraints.gridx = 1; myconstraints.gridy = 4;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1; myconstraints.weighty = 1;
	mylayout.setConstraints(messages, myconstraints);
	p1.add(messages);

	drawcanvas = new Label("Drawing canvas:");
	myconstraints.gridx = 1; myconstraints.gridy = 5;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(drawcanvas, myconstraints);
	p1.add(drawcanvas);

        canvas = new chitchatcanvas(this);
	myconstraints.gridx = 1; myconstraints.gridy = 6;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.insets.top = 5; myconstraints.insets.bottom = 5;
	myconstraints.insets.right = 5; myconstraints.insets.left = 5;
	myconstraints.weightx = 1; myconstraints.weighty = 2;
	mylayout.setConstraints(canvas, myconstraints);
	p1.add(canvas);

	myconstraints.gridx = 1; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.anchor = myconstraints.NORTHWEST;  
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1; myconstraints.weighty = 1;
	mylayout.setConstraints(p1, myconstraints);
	add(p1);

	try {
	    URL imageUrl =
                new URL(chitchatURL.getProtocol(), chitchatURL.getHost(),
                chitchatURL.getFile() + "chitchatpic.jpg");
	    Image image = getToolkit().getImage(imageUrl);
	    canvas.setimage(image);
	}
	catch (Exception e) { 
	    System.out.println(e);
	}

	// the second (right side) panel

	p2 = new Panel();
	p2.setLayout(mylayout);
	myconstraints.insets.top = 0; myconstraints.insets.bottom = 0;
	myconstraints.insets.right = 5; myconstraints.insets.left = 5;

	namelabel = new Label("User name:");
	namelabel.setFont(smallfont);
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(namelabel, myconstraints);
	p2.add(namelabel);

	userid = new TextField(name);
	userid.setFont(smallfont);
	userid.setEditable(false);
	myconstraints.gridx = 0; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(userid, myconstraints);
	p2.add(userid);

	activitylabel = new Label("Current activity:");
	activitylabel.setFont(smallfont);
	myconstraints.gridx = 0; myconstraints.gridy = 3;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(activitylabel, myconstraints);
	p2.add(activitylabel);

	activity = new TextField();
	activity.setEditable(false);
	activity.setFont(smallfont);
	myconstraints.gridx = 0; myconstraints.gridy = 4;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(activity, myconstraints);
	p2.add(activity);

	sendtolabel = new Label("Currently sending to:");
	sendtolabel.setFont(smallfont);
	myconstraints.gridx = 0; myconstraints.gridy = 5;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(sendtolabel, myconstraints);
	p2.add(sendtolabel);

	sendto = new java.awt.List();
	sendto.setFont(smallfont);
	sendto.addItemListener(this);
	sendto.add("Everyone");
	sendto.setMultipleMode(false);
	sendto.select(0);
	myconstraints.gridx = 0; myconstraints.gridy = 6;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 1.0;
	mylayout.setConstraints(sendto, myconstraints);
	p2.add(sendto);

	whosthis = new Button("Who's this?");
	whosthis.setFont(smallfont);
	whosthis.addActionListener(this);
	whosthis.setEnabled(false);
	myconstraints.gridx = 0; myconstraints.gridy = 7;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(whosthis, myconstraints);
	p2.add(whosthis);

	page = new Button("Page user(s)");
	page.setFont(smallfont);
	page.addActionListener(this);
	page.setEnabled(false);
	myconstraints.gridx = 0; myconstraints.gridy = 8;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(page, myconstraints);
	p2.add(page);

	message = new Button("Leave message");
	message.setFont(smallfont);
	message.addActionListener(this);
	message.setEnabled(false);
	myconstraints.gridx = 0; myconstraints.gridy = 9;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(message, myconstraints);
	p2.add(message);

	readmess = new Button("Read message(s)");
	readmess.setFont(smallfont);
	readmess.addActionListener(this);
	readmess.setEnabled(false);
	myconstraints.gridx = 0; myconstraints.gridy = 10;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(readmess, myconstraints);
	p2.add(readmess);

	drawingcontrols = new Label("Drawing controls:");
	drawingcontrols.setFont(smallfont);
	myconstraints.gridx = 0; myconstraints.gridy = 11;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(drawingcontrols, myconstraints);
	p2.add(drawingcontrols);

	clearcanvas = new Button("Clear canvas");
	clearcanvas.setFont(smallfont);
	clearcanvas.addActionListener(this);
	clearcanvas.setEnabled(true);
	myconstraints.gridx = 0; myconstraints.gridy = 12;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(clearcanvas, myconstraints);
	p2.add(clearcanvas);

	colorchoice = new Choice();
	colorchoice.setFont(smallfont);
	colorchoice.addItemListener(this);
	colorchoice.addItem("black");
	colorchoice.addItem("blue");
	colorchoice.addItem("cyan");
	colorchoice.addItem("dark gray");
	colorchoice.addItem("gray");
	colorchoice.addItem("green");
	colorchoice.addItem("light gray");
	colorchoice.addItem("magenta");
	colorchoice.addItem("orange");
	colorchoice.addItem("pink");
	colorchoice.addItem("red");
	colorchoice.addItem("white");
	colorchoice.addItem("yellow");
	myconstraints.gridx = 0; myconstraints.gridy = 13;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(colorchoice, myconstraints);
	p2.add(colorchoice);

	thickness = new Choice();
	thickness.setFont(smallfont);
	thickness.addItemListener(this);
	thickness.addItem("thickness: 1");
	thickness.addItem("thickness: 2");
	thickness.addItem("thickness: 3");
	thickness.addItem("thickness: 4");
	thickness.addItem("thickness: 5");
	thickness.addItem("thickness: 6");
	thickness.addItem("thickness: 7");
	thickness.addItem("thickness: 8");
	thickness.addItem("thickness: 9");
	myconstraints.gridx = 0; myconstraints.gridy = 14;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(thickness, myconstraints);
	p2.add(thickness);
	thickness.setEnabled(true);

	filltype = new Choice();
	filltype.setFont(smallfont);
	filltype.addItemListener(this);
	filltype.addItem("outlined");
	filltype.addItem("filled");
	myconstraints.gridx = 0; myconstraints.gridy = 15;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 1.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(filltype, myconstraints);
	p2.add(filltype);
	filltype.setEnabled(false);

	drawtype = new CheckboxGroup();

	freehand = new Checkbox("Draw freehand", drawtype, true);
	freehand.setFont(smallfont);
	freehand.addItemListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 16;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(freehand, myconstraints);
	p2.add(freehand);

	line = new Checkbox("Line", drawtype, false);
	line.setFont(smallfont);
	line.addItemListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 17;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(line, myconstraints);
	p2.add(line);

	oval = new Checkbox("Oval", drawtype, false);
	oval.setFont(smallfont);
	oval.addItemListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 18;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(oval, myconstraints);
	p2.add(oval);

	rectangle = new Checkbox("Rectangle", drawtype, false);
	rectangle.setFont(smallfont);
	rectangle.addItemListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 19;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(rectangle, myconstraints);
	p2.add(rectangle);

	text = new Checkbox("Text",drawtype,false);
	text.setFont(smallfont);
	text.addItemListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 20;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 2;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(text, myconstraints);
	p2.add(text);
                                                
	myconstraints.gridx = 2; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	myconstraints.weightx = 0.0; myconstraints.weighty = 0.0;
	mylayout.setConstraints(p2, myconstraints);
	add(p2);

	// register to receive the various events
	addKeyListener(this);
	addMouseListener(this);
	addWindowListener(this);

	// Show the conference text or not?
	showmessages(showMessages);
	
	// Show the drawing canvas or not?
	showcanvas(showCanvas);
	
        // show the window 
	pack();
	setVisible(true);
	offline();
	typed.requestFocus();
    }


    public void incoming(String instring)
    { 
        // This routine interprets all messages that  from the server

	String whofrom = "";
	String printstring = "";
	boolean privatemessage = false;
	int count = 0;

        // check for null
	if (instring == null)
	    return;

	if (instring.startsWith("/FROM "))
	    {
		instring = instring.substring(6);

		// Get who the message/command is from.
		whofrom = instring.substring(0, instring.indexOf(" /"));

		// if the message is from me, skip it
		if (whofrom.equals(name))
		    return;

		instring = instring.substring(instring.indexOf(" /") + 1);

		if (instring.startsWith("/TO "))
		    {
			instring = instring.substring(4);

			// If this message was not for "ALL", then it is
                        // a private message for 
			if (!instring.startsWith("ALL "))
			    privatemessage = true;

                        instring =instring.substring(instring.indexOf(" /") + 1);
		    }

		if (instring.startsWith("/CLEARCANVAS"))
		    {
			messages.append("<<" + whofrom 
					    + " cleared the canvas>>\n");
			canvas.graphics.removeAllElements();
			canvas.repaint();
			return;
		    }

		if (instring.startsWith("/TEXT "))
		    {
			// This is an ordinary text message
			instring = instring.substring(6);
			printstring = "";

                        // Finish constructing the string 
			if (privatemessage)
                            printstring += "*private from " +  whofrom + "*> ";
			else
			    // This message is public
			    printstring += whofrom + "> ";
			printstring += instring + "\n";

			// Empty the activity field
			activity.setText("");

                        // Print the string
			messages.append(printstring);
			return;
		    }

		if (instring.startsWith("/PAGE"))
		    {
			messages.append("<<" + whofrom +
					" is paging you>>\n");
			
                        // Check if the has user got sounds enabled
			if (playsound.getState())
			    {
				try {
				    URL soundURL =
                                        new URL(chitchatURL.getProtocol(),
                                                chitchatURL.getHost(),
                                                chitchatURL.getFile() +
                                                "chitchatpage.au");
				    Applet.newAudioClip(soundURL).play();
				}
				catch (Exception argh) { 
				    System.out.print(
					     "Can't play paging sound (" +
					     argh + ")\n");
				    myclient.outgoing("/FROM " + name
						      + " /NOSOUND\n");
				}
			    }
			else
			    myclient.outgoing("/FROM " + name + " /NOPAGE\n");

			return;
		    }

		if (instring.startsWith("/TYP"))
		    {
			activity.setText("typing: " + whofrom);
			return;
		    }

		if (instring.startsWith("/NOPAGE"))
		    {
			messages.append("<<user " + whofrom
					+ " has turned off paging>>\n");
			return;
		    }

		if (instring.startsWith("/NOSOUND"))
		    {
			messages.append("<<" + whofrom + "'s chat client "
					+ "cannot play sound>>\n");
			return;
		    }

		activity.setText("drawing: " + whofrom);

		String color = new String();
		String number = new String();
		String font = new String();
		String text = new String();

		Color drawcolor = Color.black;
		Font drawfont = new Font("Helvetica", Font.PLAIN, 10);
		int startx = 0;
		int starty = 0;
		int paramx = 0;
		int paramy = 0;
		int size = 0;
		int style = 0;
		boolean filled = false;

		if ((instring.startsWith("/LINE ")) ||
		    (instring.startsWith("/RECT ")) ||
		    (instring.startsWith("/OVAL ")))
		    {
			String origstring = instring;
			instring = instring.substring(6);

			// get the color
			color = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    color += instring.charAt(count);
			instring = instring.substring(count);

			drawcolor = canvas.parseColorString(color);

			// Discard 1 space character
			instring = instring.substring(1);


			// get the starting x coordinate
			    
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) == ' ')); count ++);
			instring = instring.substring(count);

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    startx = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the starting y coordinate

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    starty = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the ending x coordinate or width

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    paramx = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the ending y coordinate or height

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    paramy = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}


			// Now draw the item

			if (origstring.startsWith("/LINE "))
			    canvas.addline(drawcolor, startx, starty,
					   paramx, paramy, 1);

			else
			    {
				// Discard 1 space character
				instring = instring.substring(1);

				// get whether it's filled
				if (instring.startsWith("true"))
				    filled = true;
				else filled = false;

				if (origstring.startsWith("/RECT "))
				    canvas.addrect(drawcolor, startx, starty,
					    paramx, paramy, filled, 1);
				
				else if (origstring.startsWith("/OVAL "))
				    canvas.addoval(drawcolor, startx, starty,
					   paramx, paramy, filled, 1);
			    }
			return;
		    }

		else if (instring.startsWith("/FONT "))
		    {
			instring = instring.substring(6);

			// get the color
			color = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    color += instring.charAt(count);
			instring = instring.substring(count);

			drawcolor = canvas.parseColorString(color);

			// Discard 1 space character
			instring = instring.substring(1);


			// get the font

			font = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    font += instring.charAt(count);
			instring = instring.substring(count);

			// Discard 1 space character
			instring = instring.substring(1);


			// get the starting x coordinate

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    startx = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the starting y coordinate

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    starty = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the size

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    size = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);


			// get the style

			number = "";
			for (count = 0; ((count < instring.length()) &&
				 (instring.charAt(count) != ' ')); count++)
			    number += instring.charAt(count);
			instring = instring.substring(count);

			try {
			    style = (int) Integer.parseInt(number); 
			} 
			catch (NumberFormatException e) {;}

			// Discard 1 space character
			instring = instring.substring(1);

			// draw the text

			text = "";
			for (count = 0; count < instring.length(); count++)
			    text += instring.charAt(count);
			instring = instring.substring(count);

			// Draw the text
			canvas.drawtext(drawcolor, startx, starty,
					new Font(font, style, size),
					text, canvas.MODE_PAINT);
			return;
		    }

		else
		    return;
	    }

	else
	    {
		if (instring.startsWith("/NEWUSER "))
		    {
			// Get the new user's name
			String newuser = instring.substring(9,
				    instring.lastIndexOf(" /MESSAGE"));


			if (newuser.equals(name))
			    return;

			else
			    synchronized (sendto) {
				sendto.add(newuser);
			    }

			// Grab the message
			instring = instring.substring(instring.lastIndexOf(
							   "/MESSAGE"));

			// Print the message
			if (instring.startsWith("/MESSAGE "))
			    {
				instring = instring.substring(9);
				messages.append(instring + "\n");
				activity.setText("");
			    }

                        // Tell the new client to add current client to its user list.
			myclient.outgoing("/ADDUSER " + name + "\n");
			return;
		    }


		else if (instring.startsWith("/ADDUSER "))
		    {
			// Get the name
			instring = instring.substring(9);

			if (instring.equals(name))
			    return;

			synchronized (sendto) {
			    // Make sure it's not already there
			    for (count = 0; count < sendto.getItemCount();
				 count ++)
				{
				    if ((sendto.getItem(count)).equals(instring))
					return;
				}
			    sendto.add(instring);
			}
			return;
		    }


		else if (instring.startsWith("/REMOVEUSER "))
		    {
			String deaduser = instring.substring(12);
			
			synchronized (sendto) {
			    for (count = 0; count < sendto.getItemCount(); 
				 count ++)
				{
				    if ((sendto.getItem(count)).equals(deaduser))
					{
					    if (sendto.isIndexSelected(count))
						sendto.select(0);
					    sendto.remove(count);
					}
				}
			}

			messages.append("<<" + deaduser 
					+ " is disconnecting>>\n");
			return;
		    }

		else if (instring.startsWith("/SERVERMESSAGE "))
		    {
			instring = instring.substring(15);

                        chitchatmessagedialog theservermessage =
                            new chitchatmessagedialog(this, "Server message",
						     true, instring);
                        //Client Disconnected
			if (instring.startsWith("You are being disconnected"))
			    disconnect();

			return;
		    }

		else if (instring.startsWith("/SAVEDMESSAGES "))
		    {
			instring = instring.substring(15);
			printstring = "";
			String MessageFrom = new String("");

			for (count = 0; ((instring.charAt(count) != '/')
					 && (count < instring.length())); 
			     count ++)
			    {
				MessageFrom += instring.charAt(count);
			    }

			instring = instring.substring(count + 4);            

			for (count = 0; count < instring.length(); count ++)
			    {
				printstring += instring.charAt(count);
			    }

                        new chitchattextdialog(this, ("Message from " 
			     + MessageFrom), printstring, 40, 10,
			      TextArea.SCROLLBARS_VERTICAL_ONLY, true);
			return;
		    }

		else
		    {
			messages.append(instring + "\n");
			return;
		    }
	    }
    }


    public void online()
    {
	connected = true;
	connect.setEnabled(false);
	disconnect.setEnabled(true);
	message.setEnabled(true);
	readmess.setEnabled(true);
	page.setEnabled(true);
	menuleavemess.setEnabled(true);
	menuviewmess.setEnabled(true);
	menupage.setEnabled(true);
	menuviewmess.setEnabled(true);
        setTitle("Chit-Chat v" +chitchat.VERSION+ " - online at " +
		      host);
	canvas.image = null;
	canvas.repaint();
	return;
    }


    public void offline()
    {
	connected = false;
	connect.setEnabled(true);
	disconnect.setEnabled(false);
	message.setEnabled(false);
	readmess.setEnabled(false);
	page.setEnabled(false);
	whosthis.setEnabled(false);
	menuleavemess.setEnabled(false);
	menuviewmess.setEnabled(false);
	menupage.setEnabled(false);
	menuviewmess.setEnabled(false);
	viewadditional.setEnabled(false);
	sendto.removeAll();
	sendto.add("Everyone");
	sendto.select(0);
        setTitle("Chit-Chat v"+ chitchat.VERSION+" - offline");
	return;
    }


    protected void connect()
    {
        // check all the settings information
	if (name.equals("") ||
	    host.equals("") ||
	    port.equals(""))
	    {
                new chitchatsettings(this);

		if (name.equals("") ||
		    host.equals("") ||
		    port.equals(""))
		    {
                        new chitchatmessagedialog(this, "Connection canceled",
			 true, "You are missing some required information!");
			return;
		    }
	    }

        // open up the socket
	try {
	    portnumber = Integer.parseInt(port);
	}
	catch (NumberFormatException n) {;}

	try {
            myclient = new chitchatclient(host, name, portnumber, this);
	}
	catch (IOException c) {
	    return;
	}

	myclient.outgoing("/NEWUSER " + name 
			  + " /MESSAGE <<user "
			  + name + " joining chat>>\n");

	online();
	return;
    }


    protected synchronized void disconnect()
    {
	// send a message to everyone
	myclient.outgoing("/REMOVEUSER " + name + "\n");

	myclient.shutdown(false);
	offline();
	return;
    }

    protected void showcanvas(boolean state)
    {
	drawcanvas.setVisible(state);
	canvas.setVisible(state);
	drawingcontrols.setVisible(state);
	clearcanvas.setVisible(state);
	colorchoice.setVisible(state);
	thickness.setVisible(state);
	filltype.setVisible(state);
	freehand.setVisible(state);
	line.setVisible(state);
	oval.setVisible(state);
	rectangle.setVisible(state);
	text.setVisible(state);
	menuclear.setEnabled(state);
			
	if (state)
	    {
		setSize((getSize()).width + 1,
			(getSize()).height + 1);
	    }
	else
	    {
		messages.setSize((messages.getSize()).width, 
				 (messages.getSize()).height);
		setSize((getSize()).width + 1, 
			(getSize()).height + 1);
	    }
	
	pack();
	return;
    }

    protected void showmessages(boolean state)
    {
	sendline.setVisible(state);
	typed.setVisible(state);
	conference.setVisible(state);
	messages.setVisible(state);

	if (state)
	    {
		setSize((getSize()).width + 1,
			(getSize()).height + 1);
	    }
	else
	    {
		canvas.setSize((canvas.getSize()).width, 
			       (canvas.getSize()).height);
		setSize((getSize()).width + 1,
			(getSize()).height + 1);
	    }

	pack();
	return;
    }

    public void actionPerformed(ActionEvent E)
    {
	// the menu items

	if (E.getSource() == connect)
	    {
		connect();
		return;
	    }

	if (E.getSource() == disconnect)
	    {
		disconnect();
		return;
	    }
    
	if (E.getSource() == buggeroff)
	    {
		if (connected == true)
		    disconnect();
		dispose();
		System.exit(0);
		return;
	    }

	// the 'page users' menu item
	if (E.getSource() == menupage)
	    {
		messages.append("<<paging user " + whofor + ">>\n");
		myclient.outgoing("/FROM " + name + " /TO "
				       + whofor + " /PAGE\n");
		return;
	    }

	// the 'leave message' menu item
	if (E.getSource() == menuleavemess)
	    {
                new chitchatleavemessage(this);
		return;
	    }

	// the 'read messages' menu item
	if (E.getSource() == menuviewmess)
	    {
		myclient.outgoing("/SAVEDMESSAGES\n");
		return;
	    }

	// the 'clear canvas' menu item
	if (E.getSource() == menuclear)
	    {
		canvas.graphics = new Vector();
		canvas.image = null;
		canvas.repaint();
		if (connected)
		    myclient.outgoing("/FROM " + name 
					   + " /CLEARCANVAS\n");
		return;
	    }

	if (E.getSource() == copy)
	    {
		buffer = messages.getSelectedText();
		paste.setEnabled(true);
		return;
	    }

	if (E.getSource() == paste)
	    {
		typed.setText(buffer);
		return;
	    }

	if (E.getSource() == settings)
	    {
                new chitchatsettings(this);
		return;
	    }

	// the 'who is' menu item
	if (E.getSource() == viewadditional)
	    {
		myclient.outgoing("/WHOIS " + whofor + "\n");
		return;
	    }

	if (E.getSource() == manual)
	    {
		try {
                    URL manualURL = new URL(chitchatURL.getProtocol(),
                                    chitchatURL.getHost(),
                                    chitchatURL.getFile() + "MANUAL.TXT");

		    BufferedReader in = new BufferedReader(
                                        new InputStreamReader(
					manualURL.openStream()));

		    String inputLine = new String("");
		    String input = new String("");

		    while ((inputLine = in.readLine()) != null) 
			input = input.concat(inputLine + "\n");
		    in.close();

                    new chitchattextdialog(this, "Chit-Chat Online Manual", 
				  input, 65, 25,
				  TextArea.SCROLLBARS_VERTICAL_ONLY, false);
		    return;
		}
		catch (IOException G) {
                    new chitchatmessagedialog(this, "Data not available", 
		     true, "The MANUAL.TXT file is could not be read!");
		    return;
		}
	    }

	/*if (E.getSource() == about)
	    {
String abouttext = new String();
		
                new chitchattextdialog(this, "About Chit-Chat", 
				      abouttext, 60, 22,
				      TextArea.SCROLLBARS_NONE, true);
		return;
	    }*/

	// the 'who is' button
	if (E.getSource() == whosthis)
	    {
		myclient.outgoing("/WHOIS " + whofor + "\n");
		return;
	    }

	// the 'read messages' button
	if (E.getSource() == readmess)
	    {
		myclient.outgoing("/SAVEDMESSAGES\n");
		return;
	    }


	// the 'clear canvas button'
	if (E.getSource() == clearcanvas)
	    {
		canvas.graphics = new Vector();
		canvas.image = null;
		canvas.repaint();
		if (connected)
		    myclient.outgoing("/FROM " + name 
					   + " /CLEARCANVAS\n");
		return;
	    }

	// the 'leave message' button
	if (E.getSource() == message)
	    {
                new chitchatleavemessage(this);
		return;
	    }

	// the 'page users' button
	if (E.getSource() == page)
	    {
		messages.append("<<paging user " + whofor + ">>\n");
		myclient.outgoing("/FROM " + name + " /TO "
				       + whofor + " /PAGE\n");
		return;
	    }
    }

    public void keyPressed(KeyEvent E)
    {
    }

    public void keyReleased(KeyEvent E)
    {
	if (E.getSource() == typed)
	    {
		// the 'enter' key in the send text field
		if (E.getKeyCode() == E.VK_ENTER) 
		    {
			// Is this a private communication?
			if (sendto.getSelectedIndex() != 0)
			    messages.append("*private to " + whofor +
					    "*> ");
			else
			    messages.append(name + "> ");

			// Print the rest.
			messages.append(typed.getText());

			if (connected == true)
			    {
				// Construct the string to send over the
				// network.
				String outstring =
				    new String("/FROM " + name +
				       " /TO " + whofor + " /TEXT " +
				       typed.getText());
			
				// Send it.
				myclient.outgoing(outstring);
			    }

			// Empty the typing and activity fields
			typed.setText("");
			activity.setText("");
			return;
		    }
		else
		    {
			activity.setText("typing: " + name);
			if (connected == true)
			    {
				// Send a message to indicate that our user is
				// busy typing something
				myclient.outgoing("/FROM " + name +
					  " /TO " + whofor + " /TYP\n");
			    }			
			return;
		    }
	    }
    }

    public void keyTyped(KeyEvent E)
    {
    }   

    public void mouseClicked(MouseEvent E)
    {
    }   

    public void mouseEntered(MouseEvent E)
    {
    }   

    public void mouseExited(MouseEvent E)
    {
    }   

    public void mousePressed(MouseEvent E)
    {
    }   

    public void mouseReleased(MouseEvent E)
    {
    }   

    public void itemStateChanged(ItemEvent E)
    {
	// the 'show messages' menu item
	if (E.getSource() == showmessages)
	    {
		showmessages(showmessages.getState());
		return;
	    }

	// the 'show canvas' menu item
	if (E.getSource() == showcanvas)
	    {
		showcanvas(showcanvas.getState());
		return;
	    }

	// the 'sendto' window
	if (E.getSource() == sendto)
	    {
		whofor = sendto.getSelectedItem();

		if (whofor == null)
		    return;
		else if (whofor.equals("Everyone"))
		    {
			whofor = "ALL";
			whosthis.setEnabled(false);
			viewadditional.setEnabled(false);
		    }
		else
		    {
			whosthis.setEnabled(true);
			viewadditional.setEnabled(true);
		    }
		return;
	    }

	// the line thicknesses
	if (E.getSource() == thickness)
	    {
		canvas.drawthickness = (thickness.getSelectedIndex() + 1);
		return;
	    }

	// the fill types
	if (E.getSource() == filltype)
	    {
		if (filltype.getSelectedIndex() == 0)
		    canvas.fill = false;

		if (filltype.getSelectedIndex() == 1)
		    canvas.fill = true;

		return;
	    }

	// the draw colors
	if (E.getSource() == colorchoice)
	    {
		if (colorchoice.getSelectedIndex() == 0)
		    {
			canvas.drawcolor = Color.black;
			canvas.colorstring = 
			    canvas.makeColorString(Color.black);
		    }
		else if (colorchoice.getSelectedIndex() == 1)
		    {
			canvas.drawcolor = Color.blue;
			canvas.colorstring = 
			    canvas.makeColorString(Color.blue);
		    }
		else if (colorchoice.getSelectedIndex() == 2)
		    {
			canvas.drawcolor = Color.cyan;
			canvas.colorstring = 
			    canvas.makeColorString(Color.cyan);
		    }
		else if (colorchoice.getSelectedIndex() == 3)
		    {
			canvas.drawcolor = Color.darkGray;
			canvas.colorstring = 
			    canvas.makeColorString(Color.darkGray);
		    }
		else if (colorchoice.getSelectedIndex() == 4)
		    {
			canvas.drawcolor = Color.gray;
			canvas.colorstring = 
			    canvas.makeColorString(Color.gray);
		    }
		else if (colorchoice.getSelectedIndex() == 5)
		    {
			canvas.drawcolor = Color.green;
			canvas.colorstring = 
			    canvas.makeColorString(Color.green);
		    }
		else if (colorchoice.getSelectedIndex() == 6)
		    {
			canvas.drawcolor = Color.lightGray;
			canvas.colorstring = 
			    canvas.makeColorString(Color.lightGray);
		    }
		else if (colorchoice.getSelectedIndex() == 7)
		    {
			canvas.drawcolor = Color.magenta;
			canvas.colorstring = 
			    canvas.makeColorString(Color.magenta);
		    }
		else if (colorchoice.getSelectedIndex() == 8)
		    {
			canvas.drawcolor = Color.orange;
			canvas.colorstring = 
			    canvas.makeColorString(Color.orange);
		    }
		else if (colorchoice.getSelectedIndex() == 9)
		    {
			canvas.drawcolor = Color.pink;
			canvas.colorstring = 
			    canvas.makeColorString(Color.pink);
		    }
		else if (colorchoice.getSelectedIndex() == 10)
		    {
			canvas.drawcolor = Color.red;
			canvas.colorstring = 
			    canvas.makeColorString(Color.red);
		    }
		else if (colorchoice.getSelectedIndex() == 11)
		    {
			canvas.drawcolor = Color.white;
			canvas.colorstring = 
			    canvas.makeColorString(Color.white);
		    }
		else if (colorchoice.getSelectedIndex() == 12)
		    {
			canvas.drawcolor = Color.yellow;
			canvas.colorstring = 
			    canvas.makeColorString(Color.yellow);
		    }

		return;
	    }            

	// the draw types
	if (E.getSource() == freehand)
	    {
		canvas.drawtype = canvas.FREEHAND;
		thickness.setEnabled(true);
		filltype.setEnabled(false);
		return;
	    }

	if (E.getSource() == line)
	    {
		canvas.drawtype = canvas.LINE;
		thickness.setEnabled(true);
		filltype.setEnabled(false);
		return;
	    }

	if (E.getSource() == oval)
	    {
		canvas.drawtype = canvas.OVAL;
		thickness.setEnabled(true);
		filltype.setEnabled(true);
		return;
	    }

	if (E.getSource() == rectangle)
	    {
		canvas.drawtype = canvas.RECTANGLE;
		thickness.setEnabled(true);
		filltype.setEnabled(true);
		return;
	    }

	if (E.getSource() == text)
	    {
		canvas.drawtype = canvas.TEXT;
		thickness.setEnabled(false);
		filltype.setEnabled(false);
		return;
	    }
    }

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
    }

    public void windowClosing(WindowEvent E)
    {
	if (connected == true)
	    disconnect();
	dispose();
	System.exit(0);
	return;
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}
