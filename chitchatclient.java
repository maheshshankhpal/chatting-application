/*
Name   :-  chitchatclient.java


Classes Included :-
	# Class chitchatclientreader
	# Class chitchatclient
*/


import java.io.*;
import java.net.*;


class chitchatclientreader
    extends Thread
{
    protected Socket clientsocket;
    protected chitchatclient theclient;
    protected BufferedReader instream;
    protected boolean stop = false;


    public chitchatclientreader(Socket s, chitchatclient c)
    {
	super("Client window socket");

	clientsocket = s;
	theclient = c;

	try {
	    instream = new BufferedReader(
		  new InputStreamReader(clientsocket.getInputStream()));
	}
	catch (IOException a) {
	    return;
	}

	start();
    }

    public void run()
    {
	String line;

	while (!stop)
	    {
		line = null;

		try {
		    line = instream.readLine();

		    if (line == null)
			continue;
		    if (line.equals(""))
			continue;
		}
		catch (IOException a) { }

		theclient.myparent.incoming(line);

		// to avoid sucking up too many resources
		yield();
	    }
    }
}


class chitchatclient
{
    protected String myhost;
    protected String myname;
    protected String mylocation;
    protected String myadditional;
    protected int myport;
    protected chitchatwindow myparent;
    protected Socket mysocket;
    protected chitchatclientreader myreader;
    protected PrintWriter outstream;


    public chitchatclient(String host, String name, int portnumber,chitchatwindow mainwindow)
	throws IOException
    {
	myhost = host;
	myname = name;
	myport = portnumber;
	myparent = mainwindow;

	// set up the client socket
	try {
	    mysocket = new Socket(myhost, myport);
	}
	catch (UnknownHostException a) {
		    
            new chitchatmessagedialog(mainwindow, "Couldn't connect", true,
				     "Couldn't find the server " + host);
	    throw(new IOException());
	}
	catch (IOException b) {
		    
            new chitchatmessagedialog(mainwindow, "Couldn't connect", true,
				     "Couldn't connect to port " + portnumber
				     + " on server " + host);
	    throw(new IOException());
	}

	// set up the reader
        myreader = new chitchatclientreader(mysocket, this);

	// Get an output stream
	outstream =
	    new PrintWriter(mysocket.getOutputStream(), true);

	mylocation = myparent.location;
	myadditional = myparent.additional;

	myparent.message.setEnabled(true);
	myparent.readmess.setEnabled(true);
	myparent.page.setEnabled(true);
	myparent.menuleavemess.setEnabled(true);
	myparent.menuviewmess.setEnabled(true);
	myparent.menupage.setEnabled(true);
	myparent.menuviewmess.setEnabled(true);

		outgoing("/NAME " + myname + "\n");
		outgoing("/LOCATION " + mylocation + "\n");
		outgoing("/ADDITIONAL " + myadditional + "\n");
    }


    public void outgoing(String out)
    {
	outstream.println(out);
    }


    public synchronized void shutdown(boolean notify)
    {
	// Shut down the reader thread
	myreader.stop = true;
	myreader = null;

	// close up my socket
	try {
	    mysocket.close();
	} 
	catch (IOException e) {
            new chitchatmessagedialog(myparent, "Communication error",
				     true, "Unable to close the connection");
	}
	
	if (notify)
	    // Make a message to the user
            new chitchatmessagedialog(myparent, "Disconnected", true,
				     ("Disconnected from " + myparent.host));
	return;
    }
}
