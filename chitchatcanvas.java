/*
Name	:-  chitchatcanvas.java

Classes Included:-
	# Class chitchattext
	# Class chitchatline
	# Class chitchatrectangle
	# Class chitchatoval
	# Class chitchatsamplecanvas
	# Class chitchatfontselect
	# Class chitchatcanvas
*/

import java.awt.*;
import java.awt.event.*;
import java.util.*;


class chitchattext
{

    public Color color;
    public Font font;
    public int x;
    public int y;
    public String text;

    public chitchattext(Color mycolor, int myx, int myy, Font myfont,
		       String mytext)
    {
	color = mycolor;
	font = myfont;
	text = mytext;
	x = myx;
	y = myy;
    }
}


class chitchatline
{

    public Color color;
    public int startx;
    public int starty;
    public int endx;
    public int endy;
    public int thickness;

    public chitchatline(Color mycolor, int mystartx, int mystarty,
		       int myendx, int myendy, int mythickness)
    {
	color = mycolor;
	startx = mystartx;
	starty = mystarty;
	endx = myendx;
	endy = myendy;
	thickness = mythickness;
    }
}


class chitchatrectangle
{

    public Color color;
    public int x;
    public int y;
    public int width;
    public int height;
    public boolean fill;
    public int thickness;

    public chitchatrectangle(Color mycolor, int myx, int myy, int mywidth,
			    int myheight, boolean myfill, int mythickness)
    {
	color = mycolor;
	x = myx;
	y = myy;
	width = mywidth;
	height = myheight;
	fill = myfill;
	thickness = mythickness;
    }
}


class chitchatoval
{

    public Color color;
    public int x;
    public int y;
    public int width;
    public int height;
    public boolean fill;
    public int thickness;

    public chitchatoval(Color mycolor, int myx, int myy, int mywidth,
		       int myheight, boolean myfill, int mythickness)
    {
	color = mycolor;
	x = myx;
	y = myy;
	width = mywidth;
	height = myheight;
	fill = myfill;
	thickness = mythickness;
    }
}


class chitchatsamplecanvas
    extends Canvas
{
    private static final int X = 300;
    private static final int Y = 75;
    
    public String text;
    public chitchatwindow main;
    
    public chitchatsamplecanvas(chitchatwindow mainwindow)
    {
	super();
	main = mainwindow;
	setBackground(Color.lightGray);
	setSize(X, Y);
	repaint();
	setVisible(true);
        text = new String("chitchat and on");
    }

    public void paint(Graphics g)
    {
	g.setColor(Color.black);
	g.setFont(new Font(main.drawfont, main.drawstyle, main.drawsize));
	if (main.drawsize > Y)
	    g.drawString(text, 0, (Y - (Y / 10)));
	else
	    g.drawString(text, 0, main.drawsize);
    }
}


class chitchatfontselect
    extends Dialog
    implements ActionListener, ItemListener, KeyListener, WindowListener
{
    private GridBagLayout mylayout;
    private GridBagConstraints myconstraints;
    private chitchatwindow myparent;
    private Font samplefont;
    private Graphics samplegraphic;

    private Panel p1;
    private Label typelabel;
    private Choice type;
    private Label sizelabel;
    private Choice size;
    private Label stylelabel;
    private Checkbox bold;
    private Checkbox italics;

    private Panel p2;
    private Label samplelabel;
    private chitchatsamplecanvas sample;
    private Label textlabel;
    private TextField text;
    private Panel p3;
    private Button ok;
    private Button cancel;


    public chitchatfontselect(Frame parent)
    {
	super(parent, "Choose font", true);
        myparent = (chitchatwindow) parent;
	mylayout = new GridBagLayout();
	myconstraints = new GridBagConstraints();
	setLayout(mylayout);

	p1 = new Panel();
	p1.setLayout(mylayout);

	myconstraints.insets = new Insets(0,5,0,5);
	myconstraints.anchor = myconstraints.WEST;

	typelabel = new Label("Font type:");
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(typelabel, myconstraints);
	p1.add(typelabel);

	sizelabel = new Label("Font size:");
	myconstraints.gridx = 1; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(sizelabel, myconstraints);
	p1.add(sizelabel);

	stylelabel = new Label("Font style:");
	myconstraints.gridx = 2; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(stylelabel, myconstraints);
	p1.add(stylelabel);

	type = new Choice();
	type.addItemListener(this);
	type.addItem("Arial / Helvetica");
	type.addItem("Times New Roman / Adobe-Times");
	type.addItem("Courier New / Courier");
	type.addItem("MS Sans Serif / Lucida");
	type.addItem("MS Sans Serif II / Lucida Typewriter");
	if (myparent.drawfont.equals("Helvetica")) type.select(0);
	if (myparent.drawfont.equals("TimesRoman")) type.select(1);
	if (myparent.drawfont.equals("Courier")) type.select(2);
	if (myparent.drawfont.equals("Dialog")) type.select(3);
	if (myparent.drawfont.equals("DialogInput")) type.select(4);
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(type, myconstraints);
	p1.add(type);

	size = new Choice();
	size.addItemListener(this);
	size.addItem("10");
	size.addItem("12");
	size.addItem("14");
	size.addItem("18");
	size.addItem("20");
	size.addItem("24");
	size.addItem("36");
	size.addItem("48");
	size.addItem("60");
	size.addItem("72");
	size.addItem("120");
	if (myparent.drawsize == 10) size.select(0);
	if (myparent.drawsize == 12) size.select(1);
	if (myparent.drawsize == 14) size.select(2);
	if (myparent.drawsize == 18) size.select(3);
	if (myparent.drawsize == 20) size.select(4);
	if (myparent.drawsize == 24) size.select(5);
	if (myparent.drawsize == 36) size.select(6);
	if (myparent.drawsize == 48) size.select(7);
	if (myparent.drawsize == 60) size.select(8);
	if (myparent.drawsize == 72) size.select(9);
	if (myparent.drawsize == 120) size.select(10);
	myconstraints.gridx = 1; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(size, myconstraints);
	p1.add(size);

	bold = new Checkbox("Bold");
	bold.addItemListener(this);
	if ((myparent.drawstyle == Font.BOLD) ||
	    (myparent.drawstyle == (Font.BOLD + Font.ITALIC)))
	    bold.setState(true);
	else bold.setState(false);
	myconstraints.gridx = 2; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(bold, myconstraints);
	p1.add(bold);

	italics = new Checkbox("Italics");
	italics.addItemListener(this);
	if ((myparent.drawstyle == Font.ITALIC) ||
	    (myparent.drawstyle == (Font.BOLD + Font.ITALIC)))
	    italics.setState(true);
	else italics.setState(false);
	myconstraints.gridx = 2; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(italics, myconstraints);
	p1.add(italics);

	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(p1, myconstraints);
	add(p1);

	p2 = new Panel();
	p2.setLayout(mylayout);

	samplelabel = new Label("Font sample:");
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(samplelabel, myconstraints);
	p2.add(samplelabel);

        sample = new chitchatsamplecanvas(myparent);
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 1;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(sample, myconstraints);
	p2.add(sample);

	textlabel = new Label("Text to insert:");
	myconstraints.gridx = 0; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(textlabel, myconstraints);
	p2.add(textlabel);

	text = new TextField(40);
	text.addKeyListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 3;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(text, myconstraints);
	p2.add(text);

	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(p2, myconstraints);
	add(p2);

	p3 = new Panel();
	p3.setLayout(mylayout);

	myconstraints.insets = new Insets(5,5,5,5);

	ok = new Button("Ok");
	ok.addActionListener(this);
	ok.addKeyListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.anchor = myconstraints.EAST;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(ok, myconstraints);
	p3.add(ok);

	cancel = new Button("Cancel");
	cancel.addActionListener(this);
	cancel.addKeyListener(this);
	myconstraints.gridx = 1; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(cancel, myconstraints);
	p3.add(cancel);

	myconstraints.gridx = 0; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(p3, myconstraints);
	add(p3);


	setSize(600,600);
	pack();
	setLocation((((((myparent.getBounds()).width) 
			    - ((getSize()).width)) / 2)
			  + ((myparent.getLocation()).x)),
			 (((((myparent.getBounds()).height)
			    - ((getSize()).height)) / 2)
			  + ((myparent.getLocation()).y)));

	sample.repaint();
	addKeyListener(this);
	addWindowListener(this);
	setResizable(false);
	setVisible(true);
	text.requestFocus();
    }
    
    private void floattext()
    {
	myparent.canvas.drafttext = 
            new chitchattext(myparent.canvas.drawcolor, myparent.canvas.oldx,
			    myparent.canvas.oldy,
			    new Font(myparent.drawfont, myparent.drawstyle,
				     myparent.drawsize), text.getText());

	myparent.canvas.floatingtext = true;
    }

    public void actionPerformed(ActionEvent E)
    {
	if (E.getSource() == ok)
	    {
		floattext();
		dispose();
		return;
	    }
	
	else if (E.getSource() == cancel)
	    {
		dispose();
		return;
	    }
    }

    public void itemStateChanged(ItemEvent E)
    {
	if (E.getSource() == bold)
	    {
		if (bold.getState())
		    myparent.drawstyle = Font.BOLD;
		else
		    myparent.drawstyle = Font.PLAIN;

		if (italics.getState())
		    myparent.drawstyle += Font.ITALIC;

		sample.repaint();
		return;
	    }
	
	else if (E.getSource() == italics)
	    {
		if (italics.getState())
		    myparent.drawstyle = Font.ITALIC;
		else
		    myparent.drawstyle = Font.PLAIN;

		if (bold.getState())
		    myparent.drawstyle += Font.BOLD;

		sample.repaint();
		return;
	    }

	else if (E.getSource() == type)
	    {
		if (type.getSelectedIndex() == 0)
		    myparent.drawfont = "Helvetica";
		if (type.getSelectedIndex() == 1)
		    myparent.drawfont = "TimesRoman";
		if (type.getSelectedIndex() == 2)
		    myparent.drawfont = "Courier";
		if (type.getSelectedIndex() == 3)
		    myparent.drawfont = "Dialog";
		if (type.getSelectedIndex() == 4)
		    myparent.drawfont = "DialogInput";
		sample.repaint();
		return;
	    }

	else if (E.getSource() == size)
	    {
		if (size.getSelectedIndex() == 0) myparent.drawsize = 10;
		if (size.getSelectedIndex() == 1) myparent.drawsize = 12;
		if (size.getSelectedIndex() == 2) myparent.drawsize = 14;
		if (size.getSelectedIndex() == 3) myparent.drawsize = 18;
		if (size.getSelectedIndex() == 4) myparent.drawsize = 20;
		if (size.getSelectedIndex() == 5) myparent.drawsize = 24;
		if (size.getSelectedIndex() == 6) myparent.drawsize = 36;
		if (size.getSelectedIndex() == 7) myparent.drawsize = 48;
		if (size.getSelectedIndex() == 8) myparent.drawsize = 60;
		if (size.getSelectedIndex() == 9) myparent.drawsize = 72;
		if (size.getSelectedIndex() == 10) myparent.drawsize = 120;
		sample.repaint();
		return;
	    }
    }

    public void keyPressed(KeyEvent E)
    {
    }

    public void keyReleased(KeyEvent E)
    {
	if (E.getKeyCode() == E.VK_ENTER)
	    {
		if ((E.getSource() == ok) ||
		    (E.getSource() == text))
		    {
			floattext();
			dispose();
			return;
		    }

		else if (E.getSource() == cancel)
		    {
			dispose();
			return;
		    }
	    }

	else if (E.getKeyCode() == E.VK_TAB)
	    {
		text.transferFocus();
		return;
	    }

	else if (E.getSource() == text)
	    {
		// We need to do the sample update after the key is released
		// or else the keystroke won't show up in the text.getText()
		// call below.
		sample.text = text.getText();
		sample.repaint();
		return;
	    }
    }

    public void keyTyped(KeyEvent E)
    {
    }   

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
    }

    public void windowClosing(WindowEvent E)
    {
	dispose();
	return;
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}


public class chitchatcanvas
    extends Canvas
    implements MouseListener, MouseMotionListener
{
    public static final int FREEHAND = 0;
    public static final int LINE = 1;
    public static final int OVAL = 2;
    public static final int RECTANGLE = 3;
    public static final int TEXT = 4;

    public static final int MODE_PAINT = 0;
    public static final int MODE_XOR = 1;

    public Color drawcolor;
    public int drawthickness = 1;
    public String colorstring;

    public int drawtype = 0;
    public boolean fill = false;
    public Font font;
    public String textstring;

    public Vector graphics;
    private boolean dragging = false;
    public int oldx = 0;
    public int oldy = 0;

    private int ovalwidth;
    private int ovalheight;
    private int rectanglewidth;
    private int rectangleheight;

    // For the draft stuff
    private chitchatline draftline;
    private chitchatrectangle draftrect;
    private chitchatoval draftoval;
    protected chitchattext drafttext;
    protected boolean floatingtext;

    private chitchatwindow myparent;

    protected Image image;


    public chitchatcanvas(chitchatwindow parent)
    {
	super();
	myparent = parent;
	graphics = new Vector(1);
	setBackground(Color.white);
	setForeground(Color.black);
	drawcolor = Color.black;
	colorstring = new String("black");
	setSize(400,125);
	setVisible(true);
	addMouseListener(this);
	addMouseMotionListener(this);
    }

    public void setimage(Image theimage)
    {
	image = theimage;
	repaint();
    }

    public void paint(Graphics g)
    {

	if (image != null)
	    {
		g.drawImage(image, 0, 0, getSize().width, getSize().height, 
			    this);
	    }

	for (int count = 0; count < graphics.size(); count ++)
	    {
                if (graphics.elementAt(count) instanceof chitchatline)
		    {
                        chitchatline templine = 
                            (chitchatline) graphics.elementAt(count);
			drawline(templine.color, templine.startx,
				 templine.starty, templine.endx,
				 templine.endy, templine.thickness,
				 this.MODE_PAINT);
		    }

                if (graphics.elementAt(count) instanceof chitchatoval)
		    {
                        chitchatoval tempoval = 
                            (chitchatoval) graphics.elementAt(count);
			drawoval(tempoval.color, tempoval.x, tempoval.y,
				 tempoval.width, tempoval.height,
				 tempoval.fill, tempoval.thickness,
				 this.MODE_PAINT);
		    }

                if (graphics.elementAt(count) instanceof chitchatrectangle)
		    {
                        chitchatrectangle temprect = 
                            (chitchatrectangle) graphics.elementAt(count);
			drawrect(temprect.color, temprect.x, temprect.y,
				 temprect.width, temprect.height,
				 temprect.fill, temprect.thickness,
				 this.MODE_PAINT);
		    }

                if (graphics.elementAt(count) instanceof chitchattext)
		    {
                        chitchattext temptext =
                            (chitchattext) graphics.elementAt(count);
			g.setColor(temptext.color);
			g.setFont(temptext.font);
			g.drawString(temptext.text, temptext.x, temptext.y);

		    }

	    }

	g.dispose();
    }

    public String makeColorString(Color aColor)
    {
        // set the drawing color string
        if (aColor == Color.black)
	    return (new String("black"));
        else if (aColor == Color.blue)
	    return (new String("blue"));
        else if (aColor == Color.cyan)
	    return (new String("cyan"));
        else if (aColor == Color.darkGray)
	    return (new String("darkGray"));
        else if (aColor == Color.gray)
	    return (new String("gray"));
        else if (aColor == Color.green)
	    return (new String("green"));
        else if (aColor == Color.lightGray)
	    return (new String("lightGray"));
        else if (aColor == Color.magenta)
	    return (new String("magenta"));
        else if (aColor == Color.orange)
	    return (new String("orange"));
        else if (aColor == Color.pink)
	    return (new String("pink"));
        else if (aColor == Color.red)
	    return (new String("red"));
        else if (aColor == Color.white)
	    return (new String("white"));
        else if (aColor == Color.yellow)
	    return (new String("yellow"));

	else
	    return (new String("black"));
    }

    public Color parseColorString(String ColorString)
    {
        if (ColorString.equals("black"))
	    return (Color.black);
        else if (ColorString.equals("blue"))
	    return (Color.blue);
        else if (ColorString.equals("cyan"))
	    return (Color.cyan);
        else if (ColorString.equals("darkGray"))
	    return (Color.darkGray);
        else if (ColorString.equals("gray"))
	    return (Color.gray);
        else if (ColorString.equals("green"))
	    return (Color.green);
        else if (ColorString.equals("lightGray"))
	    return (Color.lightGray);
        else if (ColorString.equals("magenta"))
	    return (Color.magenta);
        else if (ColorString.equals("orange"))
	    return (Color.orange);
        else if (ColorString.equals("pink"))
	    return (Color.pink);
        else if (ColorString.equals("red"))
	    return (Color.red);
        else if (ColorString.equals("white"))
	    return (Color.white);
        else if (ColorString.equals("yellow"))
	    return (Color.yellow);

	else
	    return (Color.black);
    }

    public void drawline(Color color, int startx, int starty, int endx,
			 int endy, int thickness, int mode)
    {
	int dx, dy;
        Graphics g = getGraphics();
        
	if (mode == this.MODE_XOR)
	    g.setXORMode(Color.white);
	else
	    g.setColor(color);
	
	if (endx > startx)
	    dx = (endx - startx);
	else
	    dx = (startx - endx);
	if (endy > starty)
	    dy = (endy - starty);
	else
	    dy = (starty - endy);

	if (dx >= dy)
	    {
		starty -= (thickness / 2);
		endy -= (thickness / 2);
	    }
	else
	    {
		startx -= (thickness / 2);
		endx -= (thickness / 2);
	    }

	for (int count = 0; count < thickness; count ++)
	    {
		g.drawLine(startx, starty, endx, endy);
		if (dx >= dy)
		    { starty++; endy++; }
		else
		    { startx++; endx++; }
	    }

        g.dispose();
    }

    public void addline(Color color, int startx, int starty, int endx,
			 int endy, int thickness)
    {
	// Draw it.
	drawline(color, startx, starty, endx, endy, thickness,
		 this.MODE_PAINT);

	// Add it to the vector
        graphics.addElement(new chitchatline(color, startx, starty, endx,
					    endy, thickness));
    }

    public void drawoval(Color color, int x, int y, int width, int height,
			 boolean filled, int thickness, int mode)
    {
        Graphics g = getGraphics();

	if (mode == this.MODE_XOR)
	    g.setXORMode(Color.white);
	else
	    g.setColor(color);

        if (filled)
	    g.fillOval(x, y, width, height);
        else
	    for (int count = 0; count < thickness; count ++)
		{
		    g.drawOval(x, y, width, height);
		    x++; y++;
		    width -= 2; height -= 2;
		}

        g.dispose();
    }

    public void addoval(Color color, int x, int y, int width, int height,
			 boolean filled, int thickness)
    {
	// Draw the oval
	drawoval(color, x, y, width, height, filled, thickness,
		 this.MODE_PAINT);

	// Add it to the vector
        graphics.addElement(new chitchatoval(color, x, y, width,
					    height, filled, thickness));
    }

    public void drawrect(Color color, int x, int y, int width, int height,
			 boolean filled, int thickness, int mode)
    {
        
        Graphics g = getGraphics();

	if (mode == this.MODE_XOR)
	    g.setXORMode(Color.white);
	else
	    g.setColor(color);

	if (filled)
	    g.fillRect(x, y, width, height);
	else
	    {
		for (int count = 0; count < thickness; count ++)
		    {
			g.drawRect(x, y, width, height);
			x++; y++;
			width -= 2; height -= 2;
		    }
	    }

        g.dispose();
    }

    public void addrect(Color color, int x, int y, int width, int height,
			boolean filled, int thickness)
    {
	// Draw the rectangle
	drawrect(color, x, y, width, height, filled, thickness,
		 this.MODE_PAINT);

	// Add it to the vector
        graphics.addElement(new chitchatrectangle(color, x, y, width,
						 height, filled, thickness));
    }

    public void drawtext(Color color, int x, int y, Font font,
			 String text, int mode)
    {
	Graphics g = getGraphics();

	if (mode == this.MODE_XOR)
	    g.setXORMode(Color.white);
	else
	    {
                graphics.addElement(new chitchattext(color, x, y, font, text));
		g.setColor(color);
	    }
    
	g.setFont(font);
	g.drawString(text, x, y);
	g.dispose();
    }                         

    public void mouseClicked(MouseEvent E)
    {
    }

    public void mouseEntered(MouseEvent E)
    {
	myparent.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }   

    public void mouseExited(MouseEvent E)
    {
	myparent.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }   

    public void mousePressed(MouseEvent E)
    {
	// Save the coordinates of the initial click
	oldx = E.getX();
	oldy = E.getY();

	// If we are doing lines, rectangles, or ovals, we will show
	// draft lines to suggest the final shape of the object
	if (drawtype == this.LINE)
	    {
                draftline = new chitchatline(drawcolor, oldx, oldy, oldx,
					    oldy, drawthickness);
		
		// Set the draw mode to XOR and draw it.
		drawline(draftline.color, draftline.startx,
			 draftline.starty, draftline.endx,
			 draftline.endy, drawthickness, this.MODE_XOR);
	    }

	else if (drawtype == this.OVAL)
	    {
                draftoval = new chitchatoval(drawcolor, oldx, oldy,
					    1, 1, false, drawthickness);
		
		// Set the draw mode to XOR and draw it.
		drawoval(draftoval.color, draftoval.x, draftoval.y,
			 draftoval.width, draftoval.height, false,
			 drawthickness, this.MODE_XOR);
	    }

	else if (drawtype == this.RECTANGLE)
	    {
                draftrect = new chitchatrectangle(drawcolor, oldx, oldy,
						 1, 1, false, drawthickness);
		
		// Set the draw mode to XOR and draw it.
		drawrect(draftrect.color, draftrect.x, draftrect.y,
			 draftrect.width, draftrect.height, false,
			 drawthickness, this.MODE_XOR);
	    }

	// Set the activity message
	myparent.activity.setText("drawing: " + myparent.name);
    }
	
    public void mouseReleased(MouseEvent E)
    {
	if (drawtype == this.LINE)
	    {
		// Erase the draft line
		drawline(draftline.color, draftline.startx, draftline.starty,
			 draftline.endx, draftline.endy, drawthickness,
			 this.MODE_XOR);

		// Add the real line to the canvas
		addline(drawcolor, oldx, oldy, E.getX(), E.getY(),
			drawthickness);
		
		// send the draw command to the socket

		if (myparent.connected)
		    {
			int nowx = E.getX();
			int nowy = E.getY();
			int dx, dy;

			if (nowx > oldx)
			    dx = (nowx - oldx);
			else
			    dx = (oldx - nowx);
			if (nowy > oldy)
			    dy = (nowy - oldy);
			else
			    dy = (oldy - nowy);

			if (dx >= dy)
			    {
				oldy -= (drawthickness / 2);
				nowy -= (drawthickness / 2);
			    }
			else
			    {
				oldx -= (drawthickness / 2);
				nowx -= (drawthickness / 2);
			    }

			for (int count = 0; count < drawthickness; count ++)
			    {
				myparent.myclient.outgoing("/FROM "
				   + myparent.name + " /TO "
				   + myparent.whofor + " /LINE "
				   + colorstring + " " + oldx + " " + oldy
				   + " " + nowx + " " + nowy + " ");

				if (dx >= dy)
				    { oldy++; nowy++; }
				else
				    { oldx++; nowx++; }
			    }
		    }

		dragging = false;
	    }

	else if (drawtype == this.OVAL)
	    {
		// Erase the draft oval
		drawoval(draftoval.color, draftoval.x, draftoval.y,
			 draftoval.width, draftoval.height, false,
			 drawthickness, this.MODE_XOR);

		if (oldx <= E.getX())
		    ovalwidth = E.getX() - oldx;
		else
		    ovalwidth = oldx - E.getX();
		if (oldy <= E.getY())
		    ovalheight = E.getY() - oldy;
		else
		    ovalheight = oldy - E.getY();

		if ((oldx <= E.getX()) && (E.getY() >= oldy))
		    { oldx = oldx; oldy = oldy; }
		else if ((oldx <= E.getX()) && (E.getY() <= oldy))
		    { oldx = oldx; oldy = E.getY(); }
		else if ((oldx >= E.getX()) && (E.getY() >= oldy))
		    { oldx = E.getX(); oldy = oldy; }
		else if ((oldx >= E.getX()) && (E.getY() <= oldy))
		    { oldx = E.getX(); oldy = E.getY(); }

		// Add the real oval to the canvas
		addoval(drawcolor, oldx, oldy, ovalwidth, ovalheight,
			fill, drawthickness);

		if (myparent.connected)
		    {
			for (int count = 0; count < drawthickness; count ++)
			    {
				myparent.myclient.outgoing("/FROM "
				   + myparent.name + " /TO " + myparent.whofor
				   + " /OVAL " + colorstring + " " + oldx
				   + " " + oldy + " " + ovalwidth + " "
				   + ovalheight + " " + fill + " ");

				oldx++; oldy++;
				ovalwidth -= 2; ovalheight -= 2;
			    }
		    }

		dragging = false;
	    }

	else if (drawtype == this.RECTANGLE)
	    {
		// Erase the draft recatngle
		drawrect(draftrect.color, draftrect.x, draftrect.y,
			 draftrect.width, draftrect.height, false,
			 drawthickness, this.MODE_XOR);

		if (oldx <= E.getX())
		    rectanglewidth = E.getX() - oldx;
		else
		    rectanglewidth = oldx - E.getX();

		if (oldy <= E.getY())
		    rectangleheight = E.getY() - oldy;
		else
		    rectangleheight = oldy - E.getY();

		if ((oldx <= E.getX()) && (E.getY() >= oldy))
		    { oldx = oldx; oldy = oldy; }
		else if ((oldx <= E.getX()) && (E.getY() <= oldy))
		    { oldx = oldx; oldy = E.getY(); }
		else if ((oldx >= E.getX()) && (E.getY() >= oldy))
		    { oldx = E.getX(); oldy = oldy; }
		else if ((oldx >= E.getX()) && (E.getY() <= oldy))
		    { oldx = E.getX(); oldy = E.getY(); }

		// Add the real rectangle to the vector
		addrect(drawcolor, oldx, oldy, rectanglewidth,
			 rectangleheight, fill, drawthickness);

		if (myparent.connected)
		    {
			for (int count = 0; count < drawthickness; count ++)
			    {
				myparent.myclient.outgoing("/FROM "
				   + myparent.name + " /TO " + myparent.whofor
				   + " /RECT " + colorstring + " " + oldx
				   + " " + oldy + " " + rectanglewidth + " "
				   + rectangleheight + " " + fill + " ");

				oldx++; oldy++;
				rectanglewidth -= 2; rectangleheight -= 2;
			    }
		    }

		dragging = false;
	    }

	else if (drawtype == this.TEXT)
	    {
		if (floatingtext)
		    {
			// The user wants to place the text (s)he created.
	
			// Erase the old draft text
			drawtext(drawcolor, drafttext.x, drafttext.y,
				 drafttext.font, drafttext.text,
				 this.MODE_XOR);

			// Set the new coordinates
			drafttext.x = E.getX();
			drafttext.y = E.getY();
		
			// Draw the permanent text
			drawtext(drawcolor, drafttext.x, drafttext.y,
				 drafttext.font, drafttext.text,
				 this.MODE_PAINT);

			// Output to the other clients
			if (myparent.connected)
			    myparent.myclient.outgoing("/FROM "
			       + myparent.name + " /TO " + myparent.whofor
			       + " /FONT " + colorstring + " "
			       + drafttext.font.getName() + " "
			       + drafttext.x + " " + drafttext.y + " "
			       + drafttext.font.getSize() + " "
			       + drafttext.font.getStyle() + " "
			       + drafttext.text);

			floatingtext = false;
		    }

		else
		    {
                        new chitchatfontselect(myparent);

			if (floatingtext)
			    // Draw a draft version
			    drawtext(drawcolor, drafttext.x, drafttext.y,
				     drafttext.font, drafttext.text,
				     this.MODE_XOR);
		    }
	    }
    }   

    public void mouseDragged(MouseEvent E)
    {
	if (drawtype == this.FREEHAND)
	    {
		drawline(drawcolor, oldx, oldy, E.getX(), E.getY(),
			 drawthickness, this.MODE_PAINT);
	
		// Add it to the vector
                graphics.addElement(new chitchatline(drawcolor, oldx, oldy,
				    E.getX(), E.getY(), drawthickness));

		// send the draw command to the socket
		if (myparent.connected)
		    {
			int nowx = E.getX();
			int nowy = E.getY();
			int dx, dy;

			if (nowx > oldx)
			    dx = (nowx - oldx);
			else
			    dx = (oldx - nowx);
			if (nowy > oldy)
			    dy = (nowy - oldy);
			else
			    dy = (oldy - nowy);
			
			if (dx >= dy)
			    {
				oldy -= (drawthickness / 2);
				nowy -= (drawthickness / 2);
			    }
			else
			    {
				oldx -= (drawthickness / 2);
				nowx -= (drawthickness / 2);
			    }

			for (int count = 0; count < drawthickness; count ++)
			    {
				myparent.myclient.outgoing("/FROM "
				   + myparent.name + " /TO "
				   + myparent.whofor + " /LINE "
				   + colorstring + " " + oldx + " " + oldy
				   + " " + nowx + " " + nowy + " ");

				if (dx >= dy)
				    { oldy++; nowy++; }
				else
				    { oldx++; nowx++; }
			    }
		    }

		oldx = E.getX();
		oldy = E.getY();
	    }

	else
	    dragging = true;

	if (drawtype == this.LINE)
	    {
		// Erase the old draft line
		drawline(draftline.color, draftline.startx, draftline.starty,
			 draftline.endx, draftline.endy, drawthickness,
			 this.MODE_XOR);

		// Draw the new draft line
		draftline.endx = E.getX();
		draftline.endy = E.getY();
		drawline(draftline.color, draftline.startx, draftline.starty,
			 draftline.endx, draftline.endy, drawthickness,
			 this.MODE_XOR);
	    }

	else if (drawtype == this.OVAL)
	    {
		// Erase the old draft oval
		drawoval(draftoval.color, draftoval.x, draftoval.y,
			 draftoval.width, draftoval.height, false, 
			 drawthickness, this.MODE_XOR);

		if (oldx <= E.getX())
		    draftoval.width = E.getX() - oldx;
		else
		    draftoval.width = oldx - E.getX();

		if (oldy <= E.getY())
		    draftoval.height = E.getY() - oldy;
		else
		    draftoval.height = oldy - E.getY();

		if ((oldx <= E.getX()) && (E.getY() >= oldy))
		    { draftoval.x = oldx; draftoval.y = oldy; }
		else if ((oldx <= E.getX()) && (E.getY() <= oldy))
		    { draftoval.x = oldx; draftoval.y = E.getY(); }
		else if ((oldx >= E.getX()) && (E.getY() >= oldy))
		    { draftoval.x = E.getX(); draftoval.y = oldy; }
		else if ((oldx >= E.getX()) && (E.getY() <= oldy))
		    { draftoval.x = E.getX(); draftoval.y = E.getY(); }

		// Draw the new draft oval
		drawoval(draftoval.color, draftoval.x, draftoval.y,
			 draftoval.width, draftoval.height, false, 
			 drawthickness, this.MODE_XOR);
	    }

	else if (drawtype == this.RECTANGLE)
	    {
		// Erase the old draft rectangle
		drawrect(draftrect.color, draftrect.x, draftrect.y,
			 draftrect.width, draftrect.height, false, 
			 drawthickness, this.MODE_XOR);

		if (oldx <= E.getX())
		    draftrect.width = E.getX() - oldx;
		else
		    draftrect.width = oldx - E.getX();

		if (oldy <= E.getY())
		    draftrect.height = E.getY() - oldy;
		else
		    draftrect.height = oldy - E.getY();

		if ((oldx <= E.getX()) && (E.getY() >= oldy))
		    { draftrect.x = oldx; draftrect.y = oldy; }
		else if ((oldx <= E.getX()) && (E.getY() <= oldy))
		    { draftrect.x = oldx; draftrect.y = E.getY(); }
		else if ((oldx >= E.getX()) && (E.getY() >= oldy))
		    { draftrect.x = E.getX(); draftrect.y = oldy; }
		else if ((oldx >= E.getX()) && (E.getY() <= oldy))
		    { draftrect.x = E.getX(); draftrect.y = E.getY(); }

		// Draw the new draft rectangle
		drawrect(draftrect.color, draftrect.x, draftrect.y,
			 draftrect.width, draftrect.height, false, 
			 drawthickness, this.MODE_XOR);
	    }
    }

    public void mouseMoved(MouseEvent E)
    {
	if (floatingtext)
	    {
		// When the user has entered some text to place on the
		// canvas, it remains sticky with the cursor until another
		// click is entered to place it.

		// Erase the old draft text
		drawtext(drawcolor, drafttext.x, drafttext.y, drafttext.font,
			 drafttext.text, this.MODE_XOR);

		// Set the new coordinates
		drafttext.x = E.getX();
		drafttext.y = E.getY();
		
		// Draw the new floating text
		drawtext(drawcolor, drafttext.x, drafttext.y, drafttext.font,
			 drafttext.text, this.MODE_XOR);
	    }
    }   
}
