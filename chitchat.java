/*
Name    :-  chitchat.java


Classes Included :-
	# Class chitchat
*/


import java.net.*;


public class chitchat
    extends Object
    implements Runnable
{
    public static final String VERSION = "1.0";

    private chitchatwindow window;
    private URL myURL = null;
    private String name = null;
    private String host = null;
    private String port = null;
    private boolean locksettings = false;
    private boolean autoconnect = false;
    private boolean showtext = true;
    private boolean showcanvas = true;


    private void usage()
    {
        System.out.println("\nChit-Chat usage:");
        System.out.println("java chitchat [-username name] "
			   + "[-servername host] [-portnumber port]");
	System.out.println("\t[-locksettings] [-autoconnect] [-hidetext] "
			   + "[-hidecanvas]");
	return;
    }

    private boolean parseArgs(String[] args)
    {
	// Loop through any command line arguments
	for (int count = 0; count < args.length; count ++)
	    {
		if (args[count].equals("-username"))
		    {
			if (++count < args.length)
			    name = args[count];
		    }

		else if (args[count].equals("-servername"))
		    {
			if (++count < args.length)
			    host = args[count];
		    }

		else if (args[count].equals("-portnumber"))
		    {
			if (++count < args.length)
			    port = args[count];
		    }

		else if (args[count].equals("-locksettings"))
		    locksettings = true;

		else if (args[count].equals("-autoconnect"))
		    autoconnect = true;

		else if (args[count].equals("-hidetext"))
		    showtext = false;

		else if (args[count].equals("-hidecanvas"))
		    showcanvas = false;

		else if (args[count].equals("-help"))
		    {
			usage();
			return (false);
		    }

		else
		    {
                        System.out.println("\nchitchat: unknown argument "
					   + args[count]);
                        System.out.println("Type 'java chitchat -help' for "
					   + "usage information");
			return (false);
		    }
	    }

	return (true);
    }

    public static void main(String[] args)
    {
        chitchat firstinstance = new chitchat(args);
	firstinstance.run();
	return;
    }

    public chitchat(String[] args)
    {
	// Get a URL to describe the invocation directory
		try 
		{
	    	myURL = new URL("file", "localhost", "./");
		}
		catch (Exception E) {
	    	System.out.println(E);
	    	System.exit(1);
		}
	
	// Parse our args.  Only continue if successful
	if (!parseArgs(args))
	    System.exit(1);

	// If "username" is blank, that's OK.  However, if the server and/or
	// port are blank, we'll supply some default ones here
	if ((host == null) || host.equals(""))
            host = "Do ChitChat";
	if ((port == null) || port.equals(""))
	    port = "12468";

	// Open the window
        window = new chitchatwindow(name, host, port, showtext, showcanvas,
				   myURL);

	// Should the user name, server name, and port name be locked
	// against user changes?
	if (locksettings)
	    window.locksettings = true;

	// Are we supposed to attempt an automatic connection?
	if (autoconnect)
	    window.connect();

	// Done
	return;
    }

    public void run()
    {
	// Nothing to do here.
	return;
    }
}

