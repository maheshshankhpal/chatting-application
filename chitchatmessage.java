/*
Name    :-  chitchatmessage.java


Classes Included :-
	# Class chitchatmessage
*/


class chitchatmessage
{
    public String isfor;
    public String from;
    public String message;


    public chitchatmessage(String whofor, String whofrom, String info)
    {
	isfor = whofor;
	from = whofrom;
	message = info;
    }

    public String toString()
    {
	return("/FOR " + isfor + "/FROM " + from + "/MESSAGE " 
	       + message + "/END");
    }
}

