/*
Name    :-  chitchatmessagedialog.java


Classes Included :-
	# Class chitchatmessagedialog
*/


import java.awt.*;
import java.awt.event.*;


class chitchatmessagedialog
    extends Dialog
    implements ActionListener, KeyListener, WindowListener
{
    protected Frame myparent;

    protected Label message;
    protected Button ok;
    protected GridBagLayout mylayout;
    protected GridBagConstraints myconstraints;


    public chitchatmessagedialog(Frame parent, String TheTitle,
				boolean IsModal, String TheMessage)
    {
	super(parent, TheTitle, IsModal);

	myparent = parent;

	mylayout = new GridBagLayout();
	myconstraints = new GridBagConstraints();

	setLayout(mylayout);

	myconstraints.insets.top = 5; myconstraints.insets.bottom = 5;
	myconstraints.insets.left = 5; myconstraints.insets.right = 5;

	message = new Label(TheMessage);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(message, myconstraints);
	add(message);

	ok = new Button("Ok");
	ok.addActionListener(this);
	ok.addKeyListener(this);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.NONE;
	mylayout.setConstraints(ok, myconstraints);
	add(ok);

	setBackground(Color.lightGray);
	setSize(500,500);
	pack();
	setResizable(false);

	setLocation((((((myparent.getBounds()).width) 
		       - ((getSize()).width)) / 2)
		     + ((myparent.getLocation()).x)),
		    (((((myparent.getBounds()).height) 
		       - ((getSize()).height)) / 2)
		     + ((myparent.getLocation()).y)));

	addKeyListener(this);
	addWindowListener(this);
	setVisible(true);
	ok.requestFocus();
    }

    public void actionPerformed(ActionEvent E)
    {
	if (E.getSource() == ok)
	    {
		dispose();
		return;
	    }
    }

    public void keyPressed(KeyEvent E)
    {
    }

    public void keyReleased(KeyEvent E)
    {
	if (E.getKeyCode() == E.VK_ENTER)
	    {
		if (E.getSource() == ok)
		    {
			dispose();
			return;
		    }
	    }
    }

    public void keyTyped(KeyEvent E)
    {
    }   

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
	dispose();
	return;
    }

    public void windowClosing(WindowEvent E)
    {
	dispose();
	return;
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}
