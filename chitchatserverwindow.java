/*
Name    :-  chitchatserverwindow.java


Classes Included :-
	# Class chitchatserverwindow
	# Class chitchatservershutdowndialog
	# Class chitchatpicturecanvas
*/





import java.awt.*;
import java.awt.event.*;
import java.net.*;


class chitchatserverwindow
    extends Frame
    implements ActionListener, WindowListener
{
    protected GridBagLayout mylayout;
    protected GridBagConstraints myconstraints;
    protected Label listening;
    protected List userlist;
    protected Button disconnect;
    protected Button disconnectall;
    protected Button shutdown;
    protected TextField stats;
    protected TextArea logwindow;
    protected chitchatpicturecanvas canvas;
    protected chitchatserver myparent;


    public chitchatserverwindow(chitchatserver parent, String Name)
    {
	super(Name);
	myparent = parent;

	mylayout = new GridBagLayout();
	myconstraints = new GridBagConstraints();
	setLayout(mylayout);

	myconstraints.insets.top = 0; myconstraints.insets.bottom = 0;
	myconstraints.insets.right = 5; myconstraints.insets.left = 5;
	myconstraints.anchor = myconstraints.WEST;
	myconstraints.fill = myconstraints.BOTH;

	listening = new Label("Listening on port " + myparent.port);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(listening, myconstraints);
	add(listening);

	userlist = new List(4, false);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 3;
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.weightx = 1; myconstraints.weighty = 0;
	mylayout.setConstraints(userlist, myconstraints);
	add(userlist);

	disconnect = new Button("Disconnect user");
	disconnect.addActionListener(this);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 1; myconstraints.gridy = 1;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(disconnect, myconstraints);
	disconnect.setEnabled(false);
	add(disconnect);

	disconnectall = new Button("Disconnect all");
	disconnectall.addActionListener(this);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 1; myconstraints.gridy = 2;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(disconnectall, myconstraints);
	disconnectall.setEnabled(false);
	add(disconnectall);

	shutdown = new Button("Shut down");
	shutdown.addActionListener(this);
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 1; myconstraints.gridy = 3;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(shutdown, myconstraints);
	shutdown.setEnabled(true);
	add(shutdown);

	myconstraints.insets.top = 5; myconstraints.insets.bottom = 5;

	stats =
	    new TextField("Connections - current: 0  peak: 0  total: 0", 40);
	stats.setEditable(false);
	myconstraints.gridwidth = 2; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 4;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(stats, myconstraints);
	add(stats);

	logwindow = new TextArea("server activity log:\n", 20, 40,
				 TextArea.SCROLLBARS_VERTICAL_ONLY);
	logwindow.setEditable(false);
	myconstraints.gridwidth = 2; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 5;
	myconstraints.weightx = 1; myconstraints.weighty = 1;
	mylayout.setConstraints(logwindow, myconstraints);
	add(logwindow);

        canvas = new chitchatpicturecanvas(this);
	myconstraints.gridwidth = 2; myconstraints.gridheight = 1; 
	myconstraints.gridx = 0; myconstraints.gridy = 6;
	myconstraints.weightx = 0; myconstraints.weighty = 0;
	mylayout.setConstraints(canvas, myconstraints);

	add(canvas);

	try {
            URL url = new URL ("file", "localhost", "chitchatpic.jpg");
	    Image image = getToolkit().getImage(url);

	    canvas.setimage(image);
	} 
	catch (Exception e) { 
	    System.out.println(e);
	}

	addWindowListener(this);
    }

    
    public void updatestats()
    {
	//Update the statistics 

	stats.setText("Connections - current: " +
		      myparent.currentconnections + "  peak: " +
		      myparent.peakconnections + "  total: " +
		      myparent.totalconnections);
	return;
    }


    public void actionPerformed(ActionEvent E)
    {
	if (E.getSource() == disconnect)
	    {
		String disconnectUser;

		synchronized (userlist) {
		    disconnectUser = userlist.getSelectedItem();
		}

		if (disconnectUser != null)
		    {
			// Loop through all of the current connections to find
			// the object that corresponds to this name
			
			for (int count = 0;
			     count < myparent.connections.size();
			     count ++)
			    {
                                chitchatclientsocket tempuser =
                                    (chitchatclientsocket)
				    myparent.connections.elementAt(count);
				
				if (tempuser.username.equals(disconnectUser))
				    myparent.disconnect(tempuser, true);
			    }
		    }
		return;
	    }

	if (E.getSource() == disconnectall)
	    {
		myparent.disconnectall(true);
		return;
	    }

	if (E.getSource() == shutdown)
	    {
		if (myparent.connections.size() > 0)
                    new chitchatservershutdowndialog(this, myparent);
		else
		    myparent.Shutdown();
		return;
	    }
    }

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
    }

    public void windowClosing(WindowEvent E)
    {
	if (myparent.connections.size() > 0)
            new chitchatservershutdowndialog(this, myparent);
	else
	    myparent.Shutdown();
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}


class chitchatservershutdowndialog
    extends Dialog
    implements ActionListener, KeyListener, WindowListener
{
    protected chitchatserverwindow myparent;
    protected chitchatserver theserver;

    protected Label message1;
    protected Label message2;
    protected Button yes;
    protected Button cancel;
    protected GridBagLayout mylayout;
    protected GridBagConstraints myconstraints;

    public chitchatservershutdowndialog(chitchatserverwindow serverwindow,
                                       chitchatserver server)
    {
	super(serverwindow, "Server shutdown", true);
	myparent = serverwindow;
	theserver = server;

	mylayout = new GridBagLayout();
	myconstraints = new GridBagConstraints();

	setLayout(mylayout);

	myconstraints.insets.top = 0; myconstraints.insets.bottom = 0;
	myconstraints.insets.left = 5; myconstraints.insets.right = 5;

	message1 = new Label("Are you sure you want to disconnect");
	message2 = new Label("all users and shut down the server?");

	myconstraints.gridwidth = 2; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.anchor = myconstraints.CENTER;
	myconstraints.fill = myconstraints.BOTH;
	mylayout.setConstraints(message1, myconstraints);
	add(message1);

	myconstraints.gridx = 0; myconstraints.gridy = 1;
	mylayout.setConstraints(message2, myconstraints);
	add(message2);

	myconstraints.fill = myconstraints.NONE;

	yes = new Button("Yes");
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 0; myconstraints.gridy = 2;
	myconstraints.anchor = myconstraints.EAST;
	mylayout.setConstraints(yes, myconstraints);
	yes.addKeyListener(this);
	yes.addActionListener(this);
	add(yes);

	cancel = new Button("Cancel");
	myconstraints.gridwidth = 1; myconstraints.gridheight = 1;
	myconstraints.gridx = 1; myconstraints.gridy = 2;
	myconstraints.anchor = myconstraints.WEST;
	mylayout.setConstraints(cancel, myconstraints);
	cancel.addKeyListener(this);
	cancel.addActionListener(this);
	add(cancel);

	setBackground(Color.lightGray);
	pack();
	setResizable(false);

	// Center it in the middle of the server window.
	setLocation((((((myparent.getBounds()).width) - 
		       ((getSize()).width)) / 2)
		     + ((myparent.getLocation()).x)),
		    (((((myparent.getBounds()).height) - 
		       ((getSize()).height)) / 2)
		     + ((myparent.getLocation()).y)));

	addKeyListener(this);
	addWindowListener(this);
	setVisible(true);
	yes.requestFocus();
    }

    public void actionPerformed(ActionEvent E)
    {
	if (E.getSource() == yes)
	    {
		dispose();
		theserver.Shutdown();
		return;
	    }

	else if (E.getSource() == cancel)
	    {
		dispose();
		return;
	    }
    }

    public void keyPressed(KeyEvent E)
    {
    }

    public void keyReleased(KeyEvent E)
    {
	if (E.getKeyCode() == E.VK_ENTER)
	    {
		if (E.getSource() == yes)
		    {
			dispose();
			theserver.Shutdown();
			return;
		    }

		else if (E.getSource() == cancel)
		    {
			dispose();
			return;
		    }
	    }
    }

    public void keyTyped(KeyEvent E)
    {
    }   

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
    }

    public void windowClosing(WindowEvent E)
    {
	dispose();
	return;
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}


class chitchatpicturecanvas
    extends Canvas
{

    private chitchatserverwindow main;
    private Image image;
    
    public chitchatpicturecanvas(chitchatserverwindow mainwindow)
    {
	super();
	main = mainwindow;
	setBackground(Color.lightGray);
	setSize(200, 75);
	repaint();
	setVisible(true);
    }

    public void paint(Graphics g)
    {
	if (image != null)
	    {
		g.drawImage(image, 0, 0, getSize().width, getSize().height, 
			    this);
	    }
    }

    public void setimage(Image theimage)
    {
	image = theimage;
	repaint();
    }
}
