/*
Name    :-  chitchatclientsocket.java


Classes Included :-
	# Class chitchatclientsocket
*/


import java.net.*;
import java.awt.*;
import java.util.*;
import java.io.*;


class chitchatclientsocket
    extends Thread
{
    protected Socket mysocket;
    protected chitchatserver myparent;
    protected BufferedReader istream;
    protected PrintWriter ostream;

    public String username;
    public String location;
    public String additional;
    protected boolean stop = false;
    protected int count;


    public chitchatclientsocket(chitchatserver parent, Socket s, 
			       ThreadGroup threadgroup)
    {
        super("Chit-Chat client " + parent.totalconnections);

	myparent = parent;
	mysocket = s;

	username = new String ("New user");
	location = new String ("Location");
	additional = new String ("None");

	// set up the streams

	try {
	    istream = new BufferedReader(new 
		InputStreamReader(mysocket.getInputStream()));
	    ostream = new PrintWriter(mysocket.getOutputStream(), true);
	} 
	catch (IOException g) {
	    myparent.Serveroutput("Error setting up client streams\n");
	    try {
		mysocket.close();
	    } 
	    catch (IOException h) {
		myparent.Serveroutput("Couldn't close socket\n");
	    }
	    System.exit(1);
	}

	// Send welcome message
        myoutput("Welcome to Chit-Chat.");
	if (myparent.currentconnections == 0) 
	    myoutput("You are the first user on line.");
	else if (myparent.currentconnections > 1)
	    myoutput("Currently there are " + myparent.currentconnections 
		     + " other users connected\n");
	else
	    myoutput("Currently there is one other user connected\n");
    }

    public void myoutput(String s)
    {
        String outstring = s;
        ostream.println(outstring);
    }

    public void run()
    {
	String line;

	while (true)
	    {
		//To avoid sucking up resources
		yield();

		line = null;

		try {
		    line = istream.readLine();
		    if (line == null)
			continue;
		    if (line.equals(""))
			continue;
		} 
		catch (IOException g) {
		    myparent.disconnect(this, false);
		    return;
		}


		// Examine the message being passed

		
		// Is this message a text message or drawing data?

		if (line.startsWith("/FROM "))
		    {
			String tmpLine = new String(line);
			// Discard the FROM
			tmpLine = tmpLine.substring(6);

			// Discard the 'from' name and the space after it.
			for (count = 0; tmpLine.charAt(count) != ' ';
			     count ++);
			tmpLine = tmpLine.substring(count + 1);

			// Is this message for anyone in particular?
			if (tmpLine.startsWith("/TO "))
			    {
				// Discard the "/TO "
				tmpLine = tmpLine.substring(4);

				// Is this message for everybody?
				if (tmpLine.startsWith("ALL"))
				    {
					// Send it to all the clients
					myparent.thewriter.clientoutput(line);
					continue;
				    }

				// Private message. Find the client for
				// whom it is meant

				for (count = 0;
				     count < myparent.connections.size();
				     count ++)
				    {
                                        chitchatclientsocket temp = 
                                            ((chitchatclientsocket) 
					     myparent.connections.elementAt(count));
				
					if (temp == null)
					    // Oops
					    continue;

					else if (tmpLine.startsWith(temp.username))
					    {
						// Send the message only to
						// the intended recipient.
						temp.myoutput(line);
					    }
				    }
			    }
			else
			    {
				myparent.thewriter.clientoutput(line);
				continue;
			    }
		    }   

		if (line.startsWith("/NEWUSER ") || 
		    line.startsWith("/ADDUSER "))
		    {
			// Just send it to all the clients
			myparent.thewriter.clientoutput(line);
			continue;
		    }

		if (line.startsWith("/REMOVEUSER "))
		    {
			myparent.disconnect(this, false);
		    }


		// Otherwise, this message is only for the server


		if (line.startsWith("/WHOIS "))
		    {
			String whothis = new String();
			line = line.substring(7);

			for (count = 0; count < line.length(); count ++)
			    whothis += line.charAt(count);

			for (count = 0; 
			     count < myparent.connections.size();
			     count ++)
			    {
                                chitchatclientsocket temp = 
                                    ((chitchatclientsocket) 
				     myparent.connections.elementAt(count));
				
				if (temp.username.equals(whothis))
				    {
					myoutput("<<user: " + temp.username);
					myoutput("  Location: "
						 + temp.location);
					myoutput("  Additional: "
						 + temp.additional + ">>\n");
					break;
				    }
			    }
			continue;
		    }

		if (line.startsWith("/SAVEDMESSAGES"))
		    {
                        chitchatmessage m;

			for (count = 0; count < myparent.messages.size(); 
			     count ++)
			    {
                                m = (chitchatmessage) 
				    myparent.messages.elementAt(count);

				if (m.isfor.equals(username))
				    {
					myoutput("/SAVEDMESSAGES " + m.from
						 + " /IS " + m.message);
                    
					myparent.messages.removeElement(m);
					count -= 1;
				    }
			    }

			myoutput("/SERVERMESSAGE End of messages");
			continue;
		    }


		if (line.startsWith("/SETMESSAGE "))
		    {
			String messagefor = new String();
			String messagefrom = new String();
			String themessage = new String();

			line = line.substring(12);
            
			for (count = 0; count < line.length(); count++)
			    {
				if (line.charAt(count) != ' ')
				    messagefor += line.charAt(count);

				else
				    {
					line = line.substring(count + 1);
					break;
				    }
			    }

			for (count = 0; count < line.length(); count++)
			    {
				if (line.charAt(count) != ' ')
				    messagefrom += line.charAt(count);
				else
				    {
					line = line.substring(count);
					break;
				    }
			    }

			for (count = 0; count < line.length(); count++)
			    {
				themessage += line.charAt(count);
			    }

            
			myparent.messages.addElement(
                             new chitchatmessage(messagefor, messagefrom, 
				themessage));

			myparent.SaveMessages();
			continue;
		    }


		if (line.startsWith("/NAME"))
		    {
			int numberof = 0;
                        chitchatmessage tempmessage;
			line = line.substring(6);

			// Make sure we don't already have a user by the
			// same name.  Loop through all of the currently
			// connected users
			for (int count = 0;
			     count < myparent.connections.size();
			     count ++)
			    {
                                chitchatclientsocket tempsocket = 
                                    ((chitchatclientsocket) 
				     myparent.connections.elementAt(count));
				
				if (tempsocket.username.equals(line))
				    {
					myoutput("<<there is already a "
						 + "user called \"" + line +
						 "\" logged in\n");
					myoutput("Please pick another name "
						 + "and reconnect>>\n");
					myparent.disconnect(this, true);
				    }
			    }

			username = line;

			if (myparent.graphics)
			    {
				myparent.mywindow.userlist.add(username);
				myparent.mywindow.disconnect.setEnabled(true);

				if (myparent.connections.size() > 1)
				    myparent.mywindow.disconnectall.setEnabled(true);
			    }

			for (count = 0; count < 
				 myparent.messages.size();
			     count ++)
			    {
                                tempmessage = (chitchatmessage)
				    myparent.messages.elementAt(count);
				if (tempmessage.isfor.equals(username))
				    numberof += 1;
			    }

			myparent.Serveroutput("New user " + username +
					      " logging on\n");
			myparent.Serveroutput("There are " 
					      + myparent.connections.size()
					      + " users connected\n");

			if (numberof == 1) 
			    myoutput("/SERVERMESSAGE You have 1 message "
				     + "waiting");

			else if (numberof > 1) 
			    myoutput("/SERVERMESSAGE You have "
				     + numberof + " messages waiting");

			continue;
		    }

		if (line.startsWith("/LOCATION "))
		    {
			line = line.substring(10);
			location = line;
			continue;
		    }

		if (line.startsWith("/ADDITIONAL "))
		    {
			line = line.substring(12);
			additional = line;
			continue;
		    }

		continue;
	    }
    }
}
