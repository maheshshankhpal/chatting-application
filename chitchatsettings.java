/*
Name  	:-  chitchatsettings.java


Classes Included :-
	# Class chitchatsettings
*/


import java.awt.*;
import java.awt.event.*;


class chitchatsettings
    extends Dialog
    implements ActionListener, KeyListener, WindowListener
{
    private chitchatwindow myparent;

    private Label namelabel;
    private TextField name;
    private Label locationlabel;
    private TextField location;
    private Label additionallabel;
    private TextArea additional;
    private Label portlabel;
    private Label portlabel2;
    private Label hostlabel;
    private TextField port;
    private TextField host;
    private Panel p1;

    private Button ok;
    private Button cancel;
    private Panel p2;

    private GridBagLayout mylayout;
    private GridBagConstraints myconstraints;


    chitchatsettings(Frame parent)
    {
	super(parent, "Connection settings", true);

        myparent = (chitchatwindow) parent;

	mylayout = new GridBagLayout();
	myconstraints = new GridBagConstraints();
	myconstraints.insets = new Insets(0,5,0,5);
	myconstraints.anchor = myconstraints.WEST;
	setLayout(mylayout);

	p1 = new Panel();
	p1.setLayout(mylayout);

	// set up the 'name' field

	namelabel = new Label("Your user name (mandatory) : ");
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(namelabel, myconstraints);
	p1.add(namelabel);

	name = new TextField(20);
	name.addKeyListener(this);
	name.setText(myparent.name);
	myconstraints.gridx = 1; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(name, myconstraints);
	p1.add(name);

	hostlabel = new Label("Server name (mandatory) :");
	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(hostlabel, myconstraints);
	p1.add(hostlabel);

	host = new TextField(20);
	host.addKeyListener(this);
	host.setText(myparent.host);
	myconstraints.gridx = 1; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(host, myconstraints);
	p1.add(host);

	portlabel = new Label("Network port (mandatory) :");
	myconstraints.gridx = 0; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(portlabel, myconstraints);
	p1.add(portlabel);

	port = new TextField(20);
	port.addKeyListener(this);
	port.setText(myparent.port);
	myconstraints.gridx = 1; myconstraints.gridy = 2;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(port, myconstraints);
	p1.add(port);

	if (myparent.connected || myparent.locksettings)
	    {
		name.setEnabled(false);
		host.setEnabled(false);
		port.setEnabled(false);
	    }
	else
	    {
		name.setEnabled(true);
		host.setEnabled(true);
		port.setEnabled(true);
	    }

	portlabel2 = new Label("(if you don't know, don't change)");
	myconstraints.gridx = 1; myconstraints.gridy = 3;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(portlabel2, myconstraints);
	p1.add(portlabel2);

	// set up the 'location' field

	locationlabel = new Label("Your location (optional) : ");
	myconstraints.gridx = 0; myconstraints.gridy = 4;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(locationlabel, myconstraints);
	p1.add(locationlabel);

	location = new TextField(20);
	location.addKeyListener(this);
	location.setText(myparent.location);
	myconstraints.gridx = 1; myconstraints.gridy = 4;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(location, myconstraints);
	p1.add(location);

	// set up the 'additional info' field

	additionallabel = new Label("Additional info (optional) : ");
	myconstraints.gridx = 0; myconstraints.gridy = 5;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(additionallabel, myconstraints);
	p1.add(additionallabel);

	additional = new TextArea(5,20);
	additional.addKeyListener(this);
	additional.setText(myparent.additional);
	myconstraints.gridx = 1; myconstraints.gridy = 5;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(additional, myconstraints);
	p1.add(additional);

	// set up the panel

	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(p1, myconstraints);
	add(p1);

	// set up a panel for the buttons

	p2 = new Panel();
	p2.setLayout(mylayout);
	myconstraints.insets = new Insets(5,5,5,5);

	ok = new Button("Ok");
	ok.addActionListener(this);
	ok.addKeyListener(this);
	myconstraints.gridx = 0; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(ok, myconstraints);
	p2.add(ok);

	cancel = new Button("Cancel");
	cancel.addActionListener(this);
	cancel.addKeyListener(this);
	myconstraints.gridx = 1; myconstraints.gridy = 0;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(cancel, myconstraints);
	p2.add(cancel);

	// set up the panel

	myconstraints.gridx = 0; myconstraints.gridy = 1;
	myconstraints.gridheight = 1; myconstraints.gridwidth = 1;
	mylayout.setConstraints(p2, myconstraints);
	add(p2);

	setSize(200,200);
	pack();
	setLocation((((((myparent.getBounds()).width)
		       - ((getSize()).width)) / 2)
		     + ((myparent.getLocation()).x)),
		    (((((myparent.getBounds()).height)
		       - ((getSize()).height)) / 2)
		     + ((myparent.getLocation()).y)));

	addKeyListener(this);
	addWindowListener(this);
	setVisible(true);
	name.requestFocus();
    }

    private boolean checkValues()
    {
	// Don't allow a "/" character in the name field, since
	// the braindead communication protocol we're using considers
	// it to be a control character
	if (name.getText().indexOf("/") != -1)
	    {
                new chitchatmessagedialog(myparent, "Invalid character", true,
					 "\"/\" is not allowed in your user "
					 + "name!");
		return (false);
	    }
	return (true);
    }

    private void setValues()
    {
	myparent.name = name.getText();
	myparent.userid.setText(myparent.name);
	myparent.location = location.getText();
	myparent.port = port.getText();
	myparent.host = host.getText();
	myparent.additional = additional.getText();
	return;
    }

    public void actionPerformed(ActionEvent E)
    {
	if (E.getSource() == ok)
	    {
		if (checkValues())
		    {
			setValues();
			dispose();
		    }
		return;
	    }

	else if (E.getSource() == cancel)
	    {
		dispose();
		return;
	    }
    }

    public void keyPressed(KeyEvent E)
    {
    }

    public void keyReleased(KeyEvent E)
    {
	if (E.getKeyCode() == E.VK_ENTER)
	    {
		if ((E.getSource() == ok) ||
		    (E.getSource() == name) ||
		    (E.getSource() == location) ||
		    (E.getSource() == port) ||
		    (E.getSource() == host) ||
		    (E.getSource() == additional))
		    {
			if (E.getKeyCode() == E.VK_ENTER)
			    {
				if (checkValues())
				    {
					setValues();
					dispose();
				    }
				return;
			    }
		    }

		else if (E.getSource() == cancel)
		    {
			if (E.getKeyCode() == E.VK_ENTER)
			    {
				dispose();
				return;
			    }
		    }
	    }

	else if (E.getSource() == additional)
	    {
		if (E.getKeyCode() == E.VK_TAB)
		    additional.transferFocus();
	    }
    }

    public void keyTyped(KeyEvent E)
    {
    }   

    public void windowActivated(WindowEvent E)
    {
    }

    public void windowClosed(WindowEvent E)
    {
    }

    public void windowClosing(WindowEvent E)
    {
	dispose();
	return;
    }

    public void windowDeactivated(WindowEvent E)
    {
    }

    public void windowDeiconified(WindowEvent E)
    {
    }

    public void windowIconified(WindowEvent E)
    {
    }

    public void windowOpened(WindowEvent E)
    {
    }
}
